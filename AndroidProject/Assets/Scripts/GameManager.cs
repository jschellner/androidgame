﻿using System.Collections.Generic;
using Units;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public static GameManager instance;

	public GameObject mapNodes;
	public GameObject playerItems;
	public Text actionPointsText;
	public GameObject topPanel;
	public Transform linePoints;
	public Transform selectedPathPoints;
	public MapNode selectedMapNode;

	internal void Conquer(MapNode node) {
		node.SetOwnerAndPublishEvent(GameManager.instance.ActivePlayerId);
		Events.InvokeConquerEvent(node);
	}

	public MapNode SelectedMapNode {
		set {
			selectedMapNode = value;
			foreach (Player player in players)
				player.unitsUi.Army = selectedMapNode.GetPlayerArmy(player.id);
		}
		get {
			return selectedMapNode;
		}
	}

	internal void RemoveUnit(Army army, Unit unit) {
		army.Units.Remove(unit);
		Events.InvokeUnitRemovedEvent(army.Location, army, unit);
		if (army.Units.Count > 0)
			return;
		army.Location.RemoveArmy(army);
		GameObject.Destroy(army.gameObject);
	}

	public List<Player> players = new List<Player>();
	private int activePlayerId = 0;
	public int ActivePlayerId {
		set {
			Player oldActivePlayer = ActivePlayer;
			if (oldActivePlayer != null) {
				oldActivePlayer.items.ForEach(i => { i.gameObject.SetActive(false); });
				oldActivePlayer.Portrait.frame.color = Color.clear;
			}
			if (value > maxPlayers)
				activePlayerId = 1;
			else
				activePlayerId = value;
			if (ActivePlayer.lost)
				return;
			ActivePlayer.Portrait.frame.color = Color.white;
			ActivePlayer.items.ForEach(i => i.gameObject.SetActive(true));
		}
		get { return activePlayerId; }
	}
	public Player ActivePlayer => GetPlayer(ActivePlayerId);
	public int maxPlayers = 2;

	private int actionPoints = 0;
	public int ActionPoints {
		set {
			Item.SelectedItem = null;
			actionPoints = value;
			actionPointsText.text = value + "";
			Events.InvokeApChangedEvent();
			if (value == 0)
				NextRound();
		}
		get { return actionPoints; }
	}

	protected void Awake() {
		instance = this;
		Events.SetupDefaultEventHandlers();
	}

	public void Start() {
		//List<Castle> castles = MapCreator.CreateMap();
		//List<Castle> castles = MapCreator.LoadFromFile("Assets/Resources/maps/map3.json");
		List<Castle> castles = MapCreator.LoadFromFile("Assets/Resources/maps/Bluterguss.json");

		Player player1 = new Player(1, Color.red, castles[0]);
		player1.firstRound = false;
		players.Add(player1);
		player1.Portrait = GameObject.Instantiate(Prefabs.instance.playerPortraitPrefab, topPanel.transform);
		player1.unitsUi = GameObject.Instantiate(Prefabs.instance.playerUnitsUiPrefab, topPanel.transform);
		castles[0].SetOwnerAndPublishEvent(1);

		Player player2 = new Player(2, Color.blue, castles[1]);
		players.Add(player2);
		player2.Portrait = GameObject.Instantiate(Prefabs.instance.playerPortraitPrefab, topPanel.transform);
		player2.unitsUi = GameObject.Instantiate(Prefabs.instance.playerUnitsUiPrefab, topPanel.transform);
		castles[1].SetOwnerAndPublishEvent(2);

		for (int i = 0; i < players.Count; i++) {
			Vector2 posPortrait = players[i].Portrait.transform.position;
			Vector2 posUnitsUi = players[i].unitsUi.transform.position;
			players[i].Portrait.transform.position = new Vector2(10 + i * 10 + i * 30, posPortrait.y + -10);
			players[i].unitsUi.transform.position = new Vector2(10 + i * 10 + i * 30, posUnitsUi.y);
		}

		foreach (MapNode node in GameObject.FindObjectsOfType<MapNode>())
			node.Initialize();

		// castles[0].SpawnUnits(player1, 1, Unit.UnitType.NORMAL);
		// castles[1].SpawnUnits(player2, 1, Unit.UnitType.NORMAL);

		NextRound();
	}

	internal void InvokeUpdateUI() {
		Events.InvokeUpdateUiEvent();
	}

	public static void NextRound() {
		CheckWinConditions();

		instance.ActivePlayerId++;
		while (instance.ActivePlayer == null || instance.ActivePlayer.lost)
			instance.ActivePlayerId++;

		instance.ActionPoints = Constants.minActionPoints;
		Player activePlayer = instance.ActivePlayer;
		if (activePlayer.firstRound) {
			activePlayer.firstRound = false;
			for (int i = 0; i < 30; i++) {
				SpawnUnits(activePlayer.castle, activePlayer, new Units.Infantry.Regular());
			}
		}

		Events.InvokeNextRoundPhase1Event();
	}

	public static Player GetPlayer(int id) {
		int realId = id - 1;
		if (realId >= instance.players.Count || realId < 0)
			return null;
		return instance.players[realId];
	}

	public static void CheckWinConditions() {
		List<Player> allPlayers = new List<Player>();
		allPlayers.AddRange(instance.players);
		instance.players.FindAll(p => p.lost).ForEach(p => allPlayers.Remove(p));
		if (allPlayers.Count == 1)
			print(allPlayers[0] + " won");
	}

	public static List<MapNode> GetMapNodes() {
		List<MapNode> nodes = new List<MapNode>();
		nodes.AddRange(instance.mapNodes.GetComponentsInChildren<MapNode>());
		return nodes;
	}

	public static Player GetNextPlayer() {
		return instance.players[instance.ActivePlayerId % instance.players.Count];
	}

	public static Player GetPreviousPlayer() {
		int previousId = instance.ActivePlayerId - 1;
		if (previousId == 0)
			previousId = instance.players.Count;
		return instance.players[previousId - 1];
	}

	public static void DecreaseActionPointsAndFireEvent(int decrement) {
		instance.ActionPoints -= decrement;
	}

	public static void SpawnUnits(MapNode node, Unit unit) {
		SpawnUnits(node, GameManager.GetPlayer(node.ownerPlayerId), unit);
	}
	public static void SpawnUnits(MapNode node, Player player, Unit unit) {
		Army army = GameObject.Instantiate<Army>(Prefabs.instance.armyPrefab);
		army.transform.position = new Vector3(node.transform.position.x, node.transform.position.y, -1);
		army.Player = player;
		army.AddUnit(unit);
		army.Location = node;
	}

	public static void UpgradeUnit(MapNode node) {
		Army currentPlayerArmy = node.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		Units.Unit unit = currentPlayerArmy.Units.Find(u => u.CanBeUpgraded);
		if (unit == null)
			return;
		currentPlayerArmy.Units.Add(unit.Upgrade());
		currentPlayerArmy.Units.Remove(unit);
		Events.InvokeUpgradeEvent(node);
	}
}
