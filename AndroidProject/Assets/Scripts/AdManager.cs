﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour, IUnityAdsListener {
    public static AdManager instance;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        Advertisement.AddListener(this);
        Advertisement.Initialize("3439378", true);
    }

    public void ShowAd() {
        //print("Ad is ready: " + Advertisement.IsReady());
        if (Advertisement.IsReady())
            Advertisement.Show();
    }

    public void OnUnityAdsReady(string placementId) {
        //print("Ad with id: " + placementId + " is ready");
    }

    public void OnUnityAdsDidError(string message) {
        //print("ad failed: " + message);
    }

    public void OnUnityAdsDidStart(string placementId) {
        //print("ad started for placement id: " + placementId);
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
        switch (showResult) {
            case ShowResult.Finished:
                // Reward the user for watching the ad to completion.
                //print("watched ad succesfull");
                break;
            case ShowResult.Skipped:
                // Do not reward the user for skipping the ad.
                //print("skipped ad");
                break;
            case ShowResult.Failed:
                //The ad did not finish due to an error.
                //print("ad failed");
                break;
        }
    }
}
