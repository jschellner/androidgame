using System;
using System.Collections.Generic;
using UnityEngine;

public class MapEditor : MonoBehaviour {

	public UnityEngine.UI.InputField filename;

	protected void Start() {
		int id = 0;
		for (float i = -5; i < 5; i += 0.5f) {
			for (float k = -4; k < 4; k += 0.5f) {
				MapCreationNode n = GameObject.Instantiate<MapCreationNode>(Prefabs.instance.mapCreationNodePrefab);
				n.id = id++;
				n.transform.position = new Vector2(i, k);
			}
		}
	}

	protected void Update() {
		Vector2 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		MouseInfo.instance.SetText(p.ToString());
	}

	public void SwitchType(string type) {
		MapCreationNode.selectedNode.Type = type;
	}

	public void Save() {
		MapData mapData = new MapData();
		foreach (MapCreationNode n in GameObject.FindObjectsOfType<MapCreationNode>())
			if (n.Type != null)
				mapData.nodes.Add(new MapCreationNodeData(n));
		System.IO.File.WriteAllText("Assets/Resources/maps/" + filename.text + ".json", JsonUtility.ToJson(mapData));
	}

	public void Load() {
		MapData mapData = JsonUtility.FromJson<MapData>(System.IO.File.ReadAllText("Assets/Resources/maps/" + filename.text + ".json"));
		Dictionary<int, MapCreationNode> nodeMapping = new Dictionary<int, MapCreationNode>();
		foreach (MapCreationNode n in GameObject.FindObjectsOfType<MapCreationNode>()) {
			nodeMapping.Add(n.id, n);
			n.Type = "null";
			n.adjacentNodes.Clear();
		}
		mapData.nodes.ForEach(n => {
			MapCreationNode n2 = nodeMapping[n.id];
			n2.Type = n.type;
			n.adjacentNodes.ForEach(adjNodeId => n2.adjacentNodes.Add(nodeMapping[adjNodeId]));
		});
		UpdateLineRenderers();
	}

	internal static void UpdateLineRenderers() {
		foreach (LineRenderer r in GameObject.FindObjectsOfType<LineRenderer>())
			GameObject.Destroy(r.gameObject);

		foreach (MapCreationNode n in GameObject.FindObjectsOfType<MapCreationNode>()) {
			foreach (MapCreationNode adjN in n.adjacentNodes) {
				LineRenderer lineRenderer = GameObject.Instantiate(Prefabs.instance.linePointPrefab);
				lineRenderer.SetPosition(0, n.transform.position);
				lineRenderer.SetPosition(1, adjN.transform.position);
			}
		}
	}

	[Serializable]
	public class MapData {
		public List<MapCreationNodeData> nodes = new List<MapCreationNodeData>();
	}

	[Serializable]
	public class MapCreationNodeData {
		public int id;
		public string type;
		public List<int> adjacentNodes = new List<int>();
		public Vector2 position;

		public MapCreationNodeData(MapCreationNode node) {
			id = node.id;
			type = node.Type;
			node.adjacentNodes.ForEach(adjN => adjacentNodes.Add(adjN.id));
			position = node.transform.position;
		}
	}

}
