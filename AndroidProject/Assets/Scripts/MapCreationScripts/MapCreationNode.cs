using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapCreationNode : MonoBehaviour, IPointerClickHandler {

	internal static MapCreationNode selectedNode;

	public Sprite castleSprite;
	public Sprite watchTowerSprite;
	public Sprite villageSprite;
	public Sprite powerUpSprite;
	public Sprite magicTowerSprite;
	public Sprite waypointSprite;
	public Sprite nullSprite;

	public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();

	private SpriteRenderer spriteRenderer;

	public int id;

	private string _type;
	public string Type {
		get { return _type; }
		set {
			_type = value;
			if (!sprites.ContainsKey(_type))
				spriteRenderer.sprite = null;
			else {
				spriteRenderer.sprite = sprites[_type];
				spriteRenderer.color = new Color(1, 1, 1, 1f);
			}
			if (_type == "null") {
				spriteRenderer.color = new Color(1, 1, 1, 0.3f);
				_type = null;
			}
		}
	}
	public List<MapCreationNode> adjacentNodes = new List<MapCreationNode>();

	protected void Awake() {
		sprites.Add("castle", castleSprite);
		sprites.Add("watchTower", watchTowerSprite);
		sprites.Add("village", villageSprite);
		sprites.Add("powerUp", powerUpSprite);
		sprites.Add("magicTower", magicTowerSprite);
		sprites.Add("waypoint", waypointSprite);
		sprites.Add("null", nullSprite);

		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		Type = "null";
	}

	public void OnPointerClick(PointerEventData eventData) {
		if (eventData.button == PointerEventData.InputButton.Left) {
			if (selectedNode != null) {
				selectedNode.spriteRenderer.color = Color.white;
				if(selectedNode.Type == null || selectedNode.Type == "null")
					selectedNode.spriteRenderer.color = new Color(1, 1, 1, 0.3f);
			}
			selectedNode = this;
			selectedNode.spriteRenderer.color = Color.green;
		}
		if (eventData.button == PointerEventData.InputButton.Right)
			Connect();
	}

	public void Connect() {
		if (selectedNode == null || selectedNode == this)
			return;
		if (selectedNode.adjacentNodes.Contains(this)) {
			selectedNode.adjacentNodes.Remove(this);
			adjacentNodes.Remove(selectedNode);
		} else {
			selectedNode.adjacentNodes.Add(this);
			adjacentNodes.Add(selectedNode);
		}
		MapEditor.UpdateLineRenderers();
	}

}
