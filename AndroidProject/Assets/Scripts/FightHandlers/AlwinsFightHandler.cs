using System;
using System.Collections.Generic;
using Units;
using UnityEngine;
using Random = UnityEngine.Random;



public class AlwinsFightHandler : IArmyFightHandler {
	public BattleLog BattleLog { get; set; }

	void IArmyFightHandler.Fight(Army attackers, Army defenders) {

		List<Units.Unit> attackerUnits = attackers.Units;
		List<Units.Unit> defenderUnits = defenders.Units;

		Tuple<int, int> attackerBattlePower = TotalAttackerBattlePower(attackerUnits, attackers.Location);
		int averageAttackerBattlePower = ((attackerBattlePower.Item1 + attackerBattlePower.Item2) / 2);
		int attackerBattlePowerPerUnit = averageAttackerBattlePower / attackerUnits.Count;
		Debug.Log("Average attacker (" + attackerUnits.Count + " units) battle power (" + attackerBattlePower + ") : " + averageAttackerBattlePower + " ~ " + attackerBattlePowerPerUnit + "/unit");

		Tuple<int, int> defenderBattlePower = TotalDefenderBattlePower(defenderUnits, defenders.Location);
		int averageDefenderBattlePower = ((defenderBattlePower.Item1 + defenderBattlePower.Item2) / 2);
		int defenderBattlePowerPerUnit = averageDefenderBattlePower / defenderUnits.Count;
		Debug.Log("Average defender (" + defenderUnits.Count + " units) battle power (" + defenderBattlePower + ") : " + averageDefenderBattlePower + " ~ " + defenderBattlePowerPerUnit + "/unit");

		int attackerDiceRoll = Random.Range(attackerBattlePower.Item1, attackerBattlePower.Item2);
		Debug.Log("Attacker rolled dice: " + attackerDiceRoll);
		int defenderDiceRoll = Random.Range(defenderBattlePower.Item1, defenderBattlePower.Item2);
		Debug.Log("Defender rolled dice: " + defenderDiceRoll);

		int diceRollDifference = attackerDiceRoll - defenderDiceRoll;
		Debug.Log("DiceRollDifference is: " + diceRollDifference);


		Tuple<int, int> casualties;

		if (AttackersWon(diceRollDifference)) {
			Debug.Log("Attackers Won!");
			if (diceRollDifference < attackerBattlePowerPerUnit) {
				diceRollDifference += Random.Range(1, attackerUnits.Count) * attackerBattlePowerPerUnit;
			}
			casualties = CalculateCasualties(attackerUnits.Count, defenderUnits.Count, diceRollDifference, attackerBattlePowerPerUnit, defenderBattlePowerPerUnit);
		} else if (AttackersAndDefendersTie(diceRollDifference)) {

			Debug.Log("Rerolling the attacker and defender dice...");
			attackerDiceRoll = Random.Range(attackerBattlePower.Item1, attackerBattlePower.Item2);
			Debug.Log("Rolled the attacker dice to: " + attackerDiceRoll + ".");
			defenderDiceRoll = Random.Range(defenderBattlePower.Item1, defenderBattlePower.Item2);
			Debug.Log("Rolled the defender dice to: " + defenderDiceRoll + ".");

			diceRollDifference = Math.Abs(attackerDiceRoll - defenderDiceRoll);
			if (diceRollDifference == 0) {
				diceRollDifference = Random.Range(0, 1) > 0 ? attackerBattlePower.Item2 : defenderBattlePower.Item2;
			}

			if (attackerUnits.Count == defenderUnits.Count) {
				if (Random.Range(0, 1) > 0) {
					casualties = CalculateCasualties(attackerUnits.Count, defenderUnits.Count, diceRollDifference, attackerBattlePowerPerUnit, defenderBattlePowerPerUnit);
				} else {
					casualties = CalculateCasualties(attackerUnits.Count, defenderUnits.Count, diceRollDifference, attackerBattlePowerPerUnit, defenderBattlePowerPerUnit);
				}
			} else {
				casualties = CalculateCasualties(attackerUnits.Count, defenderUnits.Count, diceRollDifference, attackerBattlePowerPerUnit, defenderBattlePowerPerUnit);
			}

		} else {
			Debug.Log("Defenders Won!");
			if (Math.Abs(diceRollDifference) < defenderBattlePowerPerUnit) {
				diceRollDifference -= Random.Range(1, defenderUnits.Count) * defenderBattlePowerPerUnit;
			}
			casualties = CalculateCasualties(defenderUnits.Count, attackerUnits.Count, diceRollDifference, defenderBattlePowerPerUnit, attackerBattlePowerPerUnit);
		}

		HandleAttackersCasualties(ref casualties, attackers);
		HandleDefendersCasualties(ref casualties, defenders);

	}
	private bool AttackersWon(float battlePowerPlusDiceDifference) {
		return battlePowerPlusDiceDifference > 0;
	}
	private bool AttackersAndDefendersTie(float battlePowerPlusDiceDifference) {
		return battlePowerPlusDiceDifference == 0;
	}

	private void HandleAttackersCasualties(ref Tuple<int, int> casualties, Army attackers) {
		int attackerCasualties = casualties.Item1;
		Debug.Log("Attacker casualties is calculated to be: " + attackerCasualties + " unit(s).");
		for (int i = 0; i < attackerCasualties; i++) {
			GameManager.instance.RemoveUnit(attackers, attackers.Units[Random.Range(0, attackers.Units.Count - 1)]);
		}
	}

	private void HandleDefendersCasualties(ref Tuple<int, int> casualties, Army defenders) {
		int defenderCasualties = casualties.Item2;
		Debug.Log("Defender casualties is calculated to be: " + defenderCasualties + " unit(s).");
		for (int i = 0; i < defenderCasualties; i++) {
			GameManager.instance.RemoveUnit(defenders, defenders.Units[Random.Range(0, defenders.Units.Count - 1)]);
		}
	}

	private Tuple<int, int> TotalAttackerBattlePower(List<Unit> attackerUnits, MapNode location) {
		int a = 0;
		int b = 0;
		foreach (Units.Unit attackerUnit in attackerUnits) {
			a += (int)(attackerUnit.AttackModifier * attackerUnit.MapNodeModifier(location) * attackerUnit.DiceRangeAttack.start);
			b += (int)(attackerUnit.AttackModifier * attackerUnit.MapNodeModifier(location) * attackerUnit.DiceRangeAttack.end);
		}
		return new Tuple<int, int>(a, b);
	}

	private Tuple<int, int> TotalDefenderBattlePower(List<Unit> defenderUnits, MapNode location) {
		int a = 0;
		int b = 0;
		foreach (Units.Unit defenderUnit in defenderUnits) {
			a += (int)(defenderUnit.DefenseModifier * defenderUnit.MapNodeModifier(location) * defenderUnit.DiceRangeAttack.start);
			b += (int)(defenderUnit.DefenseModifier * defenderUnit.MapNodeModifier(location) * defenderUnit.DiceRangeAttack.end);
		}
		return new Tuple<int, int>(a, b);
	}

	private static Tuple<int, int> CalculateCasualties(int winnerUnitsCount, int loserUnitsCount, int diceRollDifference, int winnerBattlePowerPerUnit, int loserBattlePowerPerUnit) {

		int winnerCasualties = Random.Range(1, Math.Abs(diceRollDifference / winnerBattlePowerPerUnit));
		Debug.Log("Winner casualties (" + diceRollDifference + "/" + winnerBattlePowerPerUnit + "): " + winnerCasualties);
		int loserCasualties = Random.Range(1, Math.Abs(diceRollDifference / loserBattlePowerPerUnit));
		Debug.Log("Loser casualties (" + diceRollDifference + "/" + loserBattlePowerPerUnit + "): " + loserCasualties);

		// winner army size larger -> max casualties = loser army size
		if (winnerUnitsCount >= loserUnitsCount) {
			if (winnerCasualties > loserUnitsCount) {
				winnerCasualties = loserUnitsCount;
			}
			if (loserCasualties > loserUnitsCount) {
				loserCasualties = loserUnitsCount;
			}
		}
		// loser army size larger -> max casualties = winner army size
		else {
			if (winnerCasualties > winnerUnitsCount) {
				winnerCasualties = winnerUnitsCount;
			}
			if (loserCasualties > winnerUnitsCount) {
				loserCasualties = winnerUnitsCount;
			}
		}

		Tuple<int, int> totalCasualties = new Tuple<int, int>(RoundDownToNextInt(winnerCasualties), RoundDownToNextInt(loserCasualties));
		Debug.Log("Total casualties: " + totalCasualties);
		return totalCasualties;
	}

	private static float FloatAbs(float value) {
		return value < 0 ? -value : value;
	}

	private static int RoundUpToNextInt(float value) {
		return (int)Math.Ceiling(value);
	}

	private static int RoundDownToNextInt(float value) {
		return (int)Math.Floor(value);
	}

}
