using System.Collections;
using System.Collections.Generic;
using Units;
using UnityEngine;

public class ChristophersFightHandler : IArmyFightHandler {
	public int FightRounds { get; set; } = 10;
	public BattleLog BattleLog { get; set; }

	public void Fight(Army attackers, Army defenders) {
		Unit attacker = null, defender = null;
		float unitFightResult = 0;
		BattleLog.ClearLog();
		for (int i = 0; i <= FightRounds; i++) {
			attacker = PickRandomUnitFromList(attackers);
			defender = PickRandomUnitFromList(defenders);

			unitFightResult = Fight(attacker, defender, defenders.Location);
			if (unitFightResult > 1) {
				GameManager.instance.RemoveUnit(defenders, defender);
			}
			if (unitFightResult < -1) {
				GameManager.instance.RemoveUnit(attackers, attacker);
			}
			if (attackers.Units.Count == 0 || defenders.Units.Count == 0) {
				break;
			}
		}

		GameManager.instance.InvokeUpdateUI();
	}

	private float Fight(Unit attacker, Unit defender, MapNode location) {
		float attackMove = attacker.RollAttackDice();
		float defenseMove = defender.RollDefenseDice();
		BattleLog.WriteLine(attacker.Token + attackMove + " vs "+ defender.Token + defenseMove + "=" + (attackMove - defenseMove));
		return attackMove - defenseMove;
	}
	private static Unit PickRandomUnitFromList(Army army) {
		return army.Units[Random.Range(0, army.Units.Count - 1)];
	}
}
