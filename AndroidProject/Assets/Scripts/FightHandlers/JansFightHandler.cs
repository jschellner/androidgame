using System.Collections;
using System.Collections.Generic;
using Units;
using UnityEngine;

public class JansFightHandler : IArmyFightHandler {

	private readonly bool ATTACKER_WINS_ON_DRAW = true;
	public BattleLog BattleLog { get; set; }

	public void Fight(Army attackers, Army defenders) {
		List<Unit> defendingUnits = new List<Unit>(defenders.Units);
		int defenseIndexAttackingArmy = attackers.Units.Count;
		int defenseIndexDefendingArmy = defendingUnits.Count;
		int maxNum = Mathf.Max(defenseIndexAttackingArmy, defenseIndexDefendingArmy);

		List<Unit> attackingCopiedUnits = new List<Unit>(attackers.Units);
		List<Unit> defendingCopiedUnits = new List<Unit>(defendingUnits);

		for (int i = 0; i < maxNum; i++) {
			if (attackingCopiedUnits.Count == 0 || defendingCopiedUnits.Count == 0)
				break;

			Unit attackingUnit = attackingCopiedUnits[0];
			attackingCopiedUnits.RemoveAt(0);
			Unit defendingUnit = defendingCopiedUnits[0];
			defendingCopiedUnits.RemoveAt(0);

			int attackingDice = attackingUnit.RollAttackDice();
			int defendingDice = defendingUnit.RollDefenseDice();

			if (attackingDice > defendingDice || ATTACKER_WINS_ON_DRAW && attackingDice == defendingDice) {
				attackingCopiedUnits.Add(attackingUnit);
				if (i < defenseIndexAttackingArmy)
					GameManager.instance.RemoveUnit(defenders, defendingUnit);
			}
			if (defendingDice > attackingDice) {
				defendingCopiedUnits.Add(defendingUnit);
				if (i < defenseIndexDefendingArmy)
					GameManager.instance.RemoveUnit(attackers, attackingUnit);

			}
			if (!ATTACKER_WINS_ON_DRAW && attackingDice == defendingDice) {
				attackingCopiedUnits.Add(attackingUnit);
				defendingCopiedUnits.Add(defendingUnit);
			}
		}
	}
}
