using UnityEngine;
using UnityEngine.UI;

public class BattleLog : MonoBehaviour {
	public Text logger;
	public void Awake() {
		this.ClearLog();
		//Events..Add(HandleNextRoundPhase2);
	}

	private void HandleNextRoundPhase2() {
		//GetComponent<Text>().color = GameManager.instance.ActivePlayer.color;
	}

	public void ClearLog() {
		logger.text = "";
	}

	public void WriteLine(string logEntry) {
		GetComponent<Text>().color = GameManager.instance.ActivePlayer.color;
		logger.text += "\n" + logEntry;
	}
}
