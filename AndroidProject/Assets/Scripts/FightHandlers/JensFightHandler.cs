using System;
using System.Collections.Generic;
using Units;
using UnityEngine;
using Random = UnityEngine.Random;



public class JensFightHandler : IArmyFightHandler {
	public BattleLog BattleLog { get; set; }

	void IArmyFightHandler.Fight(Army attackers, Army defenders) {

		List<Units.Unit> attackerUnits = attackers.Units;
		List<Units.Unit> defenderUnits = defenders.Units;

		Tuple<int, int> attackerBattlePower = TotalAttackerBattlePower(attackerUnits, attackers.Location);
		Debug.Log("Total attacker-BP (" + attackerUnits.Count + " units) : " + attackerBattlePower);

		Tuple<int, int> defenderBattlePower = TotalDefenderBattlePower(defenderUnits, defenders.Location);
		Debug.Log("Total defender-BP (" + defenderUnits.Count + " units) : " + defenderBattlePower);

		int attackerDiceRoll = Random.Range(attackerBattlePower.Item1, attackerBattlePower.Item2);
		Debug.Log("Attacker rolled dice: " + attackerDiceRoll);
		int defenderDiceRoll = Random.Range(defenderBattlePower.Item1, defenderBattlePower.Item2);
		Debug.Log("Defender rolled dice: " + defenderDiceRoll);


		float attackerCasualtiesPercentage = defenderDiceRoll / (float)(attackerDiceRoll + defenderDiceRoll);
		if (attackerCasualtiesPercentage >= 0.9f) {
			attackerCasualtiesPercentage = 1f;
		} else if (attackerCasualtiesPercentage <= 0.1f) {
			attackerCasualtiesPercentage = 0f;
		}
		Debug.Log("Attacker casualties percentage = " + attackerCasualtiesPercentage);
		float defenderCasualtiesPercentage = attackerDiceRoll / (float)(attackerDiceRoll + defenderDiceRoll);
		if (defenderCasualtiesPercentage >= 0.9f) {
			defenderCasualtiesPercentage = 1f;
		} else if (defenderCasualtiesPercentage <= 0.1f) {
			defenderCasualtiesPercentage = 0f;
		}
		Debug.Log("Defender casualties percentage = " + defenderCasualtiesPercentage);

		int attackerCasualties = RoundUpToNextInt(attackerCasualtiesPercentage * attackerUnits.Count);
		if (attackerCasualties > attackerUnits.Count) {
			attackerCasualties = attackerUnits.Count;
		}
		int defenderCasualties = RoundUpToNextInt(defenderCasualtiesPercentage * defenderUnits.Count);
		if (defenderCasualties > defenderUnits.Count) {
			defenderCasualties = defenderUnits.Count;
		}

		BattleLog.ClearLog();
		BattleLog.WriteLine("");
		BattleLog.WriteLine("Attacker: " + attackerBattlePower + " (" + attackerUnits.Count + " units) rolled: " + attackerDiceRoll + ", casualties= " + attackerCasualties + " (" + attackerCasualtiesPercentage + ")");
		BattleLog.WriteLine("Defender: " + defenderBattlePower + " (" + defenderUnits.Count + " units) rolled: " + defenderDiceRoll + ", casualties= " + defenderCasualties + " (" + defenderCasualtiesPercentage + ")");

		HandleAttackersCasualties(attackerCasualties, attackers);
		HandleDefendersCasualties(defenderCasualties, defenders);

	}

	private void HandleAttackersCasualties(int casualties, Army attackers) {
		Debug.Log("Attacker casualties is calculated to be: " + casualties + " unit(s).");
		for (int i = 0; i < casualties; i++) {
			GameManager.instance.RemoveUnit(attackers, attackers.Units[Random.Range(0, attackers.Units.Count - 1)]);
		}
	}

	private void HandleDefendersCasualties(int casualties, Army defenders) {
		Debug.Log("Defender casualties is calculated to be: " + casualties + " unit(s).");
		for (int i = 0; i < casualties; i++) {
			GameManager.instance.RemoveUnit(defenders, defenders.Units[Random.Range(0, defenders.Units.Count - 1)]);
		}
	}

	private Tuple<int, int> TotalAttackerBattlePower(List<Unit> attackerUnits, MapNode location) {
		int a = 0;
		int b = 0;
		foreach (Units.Unit attackerUnit in attackerUnits) {
			a += (int)(attackerUnit.AttackModifier * attackerUnit.MapNodeModifier(location) * attackerUnit.DiceRangeAttack.start);
			b += (int)(attackerUnit.AttackModifier * attackerUnit.MapNodeModifier(location) * attackerUnit.DiceRangeAttack.end);
		}
		return new Tuple<int, int>(a, b);
	}

	private Tuple<int, int> TotalDefenderBattlePower(List<Unit> defenderUnits, MapNode location) {
		int a = 0;
		int b = 0;
		foreach (Units.Unit defenderUnit in defenderUnits) {
			a += (int)(defenderUnit.DefenseModifier * defenderUnit.MapNodeModifier(location) * defenderUnit.DiceRangeAttack.start);
			b += (int)(defenderUnit.DefenseModifier * defenderUnit.MapNodeModifier(location) * defenderUnit.DiceRangeAttack.end);
		}
		return new Tuple<int, int>(a, b);
	}

	private static int RoundUpToNextInt(float value) {
		return (int)Math.Ceiling(value);
	}

}
