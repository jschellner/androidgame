﻿using System.Collections.Generic;
using UnityEngine;

namespace Units {
	public abstract class Unit {
		[SerializeReference]
		private RangeInt _diceRangeAttack = new RangeInt(1, 6);
		public RangeInt DiceRangeAttack {
			get { return _diceRangeAttack; }
			protected set { _diceRangeAttack = value; }
		}
		[SerializeReference]
		private RangeInt _diceRangeDefense = new RangeInt(1, 6);
		public RangeInt DiceRangeDefense {
			get { return _diceRangeDefense; }
			protected set { _diceRangeDefense = value; }
		}

		[SerializeField]
		private bool _canBeUpgraded = false;
		public bool CanBeUpgraded {
			get { return _canBeUpgraded; }
			protected set { _canBeUpgraded = value; }
		}

		public bool IsMagicImmune { get; protected set; } = false;
		public float AttackModifier { get; protected set; } = 1;
		public float DefenseModifier { get; protected set; } = 1;
		public int CurrentHP { get; protected set; } = 100;
		public int MaxHP { get; protected set; } = 100;

		public abstract string Token { get; }
		public bool CanMove { get; protected set; } = true;

		internal float MapNodeModifier(MapNode location) {
			return 1.0f;
		}

		public int RollAttackDice() {
			return Random.Range(DiceRangeAttack.start, DiceRangeAttack.length + 1);
		}

		public int RollDefenseDice() {
			return Random.Range(DiceRangeDefense.start, DiceRangeDefense.length + 1);
		}

		public abstract Unit Upgrade();

		internal object CompareTo(Unit u2) {
			throw new System.NotImplementedException();
		}

		public static implicit operator Unit(List<Unit> v) {
			throw new System.NotImplementedException();
		}
	}
}