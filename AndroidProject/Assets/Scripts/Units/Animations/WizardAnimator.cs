using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class WizardAnimator : AnimatorBase {
	private const float COLOR_CHANGE_TIME = 1f;

	public Light2D magicBall;

	private float magicBallTimeElapsed = 0f;
	private List<Color> magicBallColors = new List<Color>();
	private int magicBallColorIndex = 0;
	private Color currentColor = Color.blue;
	private Color targetColor = Color.green;
	private bool hardSetColor = false;

	protected override void Awake() {
		base.Awake();
		magicBallColors.Add(Color.blue);
		magicBallColors.Add(Color.green);
		magicBallColors.Add(Color.red);
		ChangeMagicBallColor();
	}

	protected override void Update() {
		base.Update();
		ChangeMagicBallColor();
	}

	private void ChangeMagicBallColor() {
		if(hardSetColor){
			magicBall.color = currentColor;
			return;
		}

		magicBall.color = Color.Lerp(currentColor, targetColor, magicBallTimeElapsed / COLOR_CHANGE_TIME);
		magicBallTimeElapsed += Time.deltaTime;

		if (magicBallTimeElapsed >= COLOR_CHANGE_TIME) {
			magicBallTimeElapsed %= COLOR_CHANGE_TIME;
			currentColor = magicBall.color;
			magicBallColorIndex = GetNextMagicBallColorIndex();
			targetColor = magicBallColors[magicBallColorIndex];
		}
	}

	private int GetNextMagicBallColorIndex() {
		int nextIndex = magicBallColorIndex + 1;
		if (nextIndex >= magicBallColors.Count)
			nextIndex = 0;
		return nextIndex;
	}

	public void HardSetColorToRed() {
		currentColor = Color.red;
		targetColor = Color.red;
		hardSetColor = true;
	}

	public void ResetHardSetColor(){
		hardSetColor = false;
	}
}
