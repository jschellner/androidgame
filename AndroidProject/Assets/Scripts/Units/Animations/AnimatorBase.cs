using UnityEngine;

public class AnimatorBase : MonoBehaviour {

	public const string IDLE = "idle";
	public const string ATTACK = "attack";
	public const string RUN = "run";

	private Animator animator;
	private string currentState;

	protected virtual void Awake() {
		animator = GetComponent<Animator>();
	}

	protected virtual void Update() {
		if (Input.GetKeyDown(KeyCode.A))
			ChangeAnimation(IDLE);
		if (Input.GetKeyDown(KeyCode.S))
			ChangeAnimation(ATTACK);
		if (Input.GetKeyDown(KeyCode.D))
			ChangeAnimation(RUN);
	}

	public void ChangeAnimation(string state) {
		if (currentState == state) return;
		animator.CrossFade(state, 0.1f);
		currentState = state;
	}
}
