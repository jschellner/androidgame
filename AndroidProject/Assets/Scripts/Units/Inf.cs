﻿using UnityEngine;

namespace Units.Infantry {
	public class Regular : Unit {

		public Regular() {
			CanBeUpgraded = true;
		}

		public override string Token { get { return "N"; } }

		public override Unit Upgrade() {
			return new Upgraded();
		}
	}
	public class Guard : Unit {
		public override string Token { get { return "G"; } }
		public Guard() {
			DiceRangeDefense = new RangeInt(4, 8);
			DefenseModifier = 1.5f;
			IsMagicImmune = true;
		}

		public override Unit Upgrade() {
			return this;
		}
	}
	public class Upgraded : Unit {
		public override string Token { get { return "U"; } }

		public Upgraded() {
			DiceRangeAttack = new RangeInt(2, 9);
			DiceRangeDefense = new RangeInt(2, 7);
			DefenseModifier = 1f;
			AttackModifier = 1.25f;
		}
		public override Unit Upgrade() {
			return this;
		}
	}
}