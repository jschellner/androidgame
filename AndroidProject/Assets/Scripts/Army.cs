﻿using System;
using System.Collections.Generic;
using System.Linq;
using Units;
using Units.Infantry;
using UnityEngine;

public class Army : MonoBehaviour {

	private readonly IArmyFightHandler fightHandler = new JensFightHandler();
	public List<MapNode> MovementTargets { get; set; } = new List<MapNode>();
	public List<Unit> Units { get; set; } = new List<Unit>();

	internal Regular GetRegularUnit() {
		return Units.Find(u => u is Units.Infantry.Regular) as Regular;
	}
	internal Upgraded GetUpgradedUnit() {
		return Units.Find(u => u is Units.Infantry.Upgraded) as Upgraded;
	}
	internal Guard GetGuardUnit() {
		return Units.Find(u => u is Units.Infantry.Guard) as Guard;
	}

	private MapNode location;
	public MapNode Location {
		set {
			if (location != null) location.RemoveArmy(this);
			if (value != null && (MovementTargets.Count == 0 || MovementTargets[MovementTargets.Count - 1].Equals(value))) value.AddArmy(this);
			location = value;
		}
		get { return location; }
	}

	private Player player;
	public Player Player {
		set {
			player = value;
			spriteRenderer.color = value.color;
		}
		get { return player; }
	}
	[SerializeField] private float speed = 2f;
	private SpriteRenderer spriteRenderer;

	protected void Awake() {
		fightHandler.BattleLog = FindObjectOfType<BattleLog>();
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	protected void Update() {
		if (MovementTargets.Count == 0)
			return;

		Location = null;
		transform.position = Vector3.MoveTowards(transform.position, MovementTargets[0].transform.position, speed * Time.deltaTime);

		if (Vector2.Distance(transform.position, MovementTargets[0].transform.position) > 0.001f)
			return;

		transform.position = new Vector3(MovementTargets[0].transform.position.x, MovementTargets[0].transform.position.y, -1);
		Location = MovementTargets[0];
		MovementTargets.RemoveAt(0);
		GameManager.DecreaseActionPointsAndFireEvent(1);

		if (MovementTargets.Count == 0)
			ArmyMover.isMoving = false;
	}

	public void AddUnit(Unit unit) {
		Units.Add(unit);
		Events.InvokeUnitAddedEvent(Location, this, unit);
	}



	public void AttackArmy(Army otherArmy) {
		fightHandler.Fight(this, otherArmy);
	}

	public void Merge(Army otherArmy) {
		otherArmy.Units.ForEach(u => AddUnit(u));
		GameObject.Destroy(otherArmy.gameObject);
	}

	public Army Split(List<Unit> splittingUnits) {
		if (splittingUnits.Count == Units.Count)
			return this;

		Army splitArmy = GameObject.Instantiate<Army>(Prefabs.instance.armyPrefab);
		splitArmy.transform.position = transform.position;
		splitArmy.Player = Player;

		foreach (Unit unit in splittingUnits) {
			GameManager.instance.RemoveUnit(this, unit);
			splitArmy.AddUnit(unit);
		}
		return splitArmy;
	}

	public Dictionary<string, List<Unit>> GetGroupedUnits() {
		return Units.GroupBy(u => u.Token).ToDictionary(group => group.Key, group => group.ToList());
	}
	public List<Unit> MovableUnits => Units.FindAll(u => u.CanMove);
	public Unit GetUnitOfType<T>(T t) where T : Type => Units.Find(u => u.GetType().Equals(t));

	public List<Unit> NonMagicImmuneUnits => Units.FindAll(u => !u.IsMagicImmune);

}