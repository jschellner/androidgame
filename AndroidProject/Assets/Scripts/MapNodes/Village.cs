﻿using System.Collections.Generic;
using UnityEngine;

public class Village : MapNode {

	public List<Sprite> smallSprites = new List<Sprite>();
	public List<Sprite> bigSprites = new List<Sprite>();


	public Village() : base() {
		Action1 = new RecruiteUnitAction(this);
		Action2 = new CreateNormalUnitAtCastle(this);
	}
	protected override void Awake() {
		base.Awake();
		spriteRenderer.sprite = smallSprites[Random.Range(0, 2)];
	}

}
