﻿using System.Collections.Generic;
using UnityEngine;

public class Castle : MapNode {


	public Castle() : base() {
		List<BasePassive> toggles = new List<BasePassive>();
		toggles.Add(new CreateUpgradedUnit(this));
		toggles.Add(new CreateGuard(this));
		Action1 = new TogglePassive(this, toggles);
		Action2 = new RecruiteGuardAction(this);
	}

	public override void Initialize() {
		Events.ownerChangedEventListeners.Add(OnOwnerChanged);
		DeployGuards();
		possibleBuildings.Add(new Barracks());
	}

	public void DeployGuards() {
		for (int i = 0; i < 60; i++) {
			GameManager.SpawnUnits(this, GameManager.GetPlayer(ownerPlayerId), new Units.Infantry.Guard());
		}
	}

	internal void OnOwnerChanged(MapNode node, int oldOwnerId, int newOwnerId) {
		if (node != this)
			return;

		Player oldOwner = GameManager.GetPlayer(oldOwnerId);
		if (oldOwner == null)
			return;

		foreach (Castle c in GameObject.FindObjectsOfType<Castle>())
			if (c.ownerPlayerId == oldOwner.id)
				return;

		oldOwner.Lose();
	}
}
