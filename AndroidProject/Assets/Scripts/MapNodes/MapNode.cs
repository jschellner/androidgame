﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class MapNode : MonoBehaviour, IPointerClickHandler {

	public AttackAction AttackAction { get; set; }
	public ConquerAction ConquerAction { get; set; }
	public BaseEffect Action1 { get; set; }
	public BaseEffect Action2 { get; set; }

	public List<BaseBuilding> possibleBuildings = new List<BaseBuilding>();

	public List<BaseBuilding> buildBuildings = new List<BaseBuilding>();


	public List<Action> attackListeners = new List<Action>();

	public List<MapNode> adjacentMapNodes = new List<MapNode>();

	public int id;
	public int ownerPlayerId;

	public readonly List<Army> armies = new List<Army>();
	public Army GetPlayerArmy(int playerId) => armies.Find(army => army.Player.id == playerId);

	public SpriteRenderer spriteRenderer;
	public MapNodeUi ui;

	public bool magicImmune = false;
	public MapNode() {
		AttackAction = new AttackAction(this);
		ConquerAction = new ConquerAction(this);
	}

	protected virtual void Awake() {
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		ui = GetComponent<MapNodeUi>();
	}
	public virtual void Initialize() {
	}

	public void Start() { }

	public void AddAdjacentNodes(params MapNode[] adjNodes) {
		foreach (MapNode adjN in adjNodes) {
			if (adjacentMapNodes.Contains(adjN) && adjN.adjacentMapNodes.Contains(this))
				continue;

			if (!adjacentMapNodes.Contains(adjN) && !adjN.adjacentMapNodes.Contains(this)) {
				LineRenderer lineRenderer = GameObject.Instantiate(Prefabs.instance.linePointPrefab, GameManager.instance.linePoints);
				lineRenderer.startColor = new Color(1f, 1f, 1f, 0.1f);
				lineRenderer.endColor = new Color(1f, 1f, 1f, 0.1f);
				lineRenderer.startWidth = 0.04f;
				lineRenderer.endWidth = 0.04f;
				lineRenderer.SetPosition(0, transform.position);
				lineRenderer.SetPosition(1, adjN.transform.position);
			}

			if (!adjacentMapNodes.Contains(adjN))
				adjacentMapNodes.Add(adjN);

			if (!adjN.adjacentMapNodes.Contains(this))
				adjN.adjacentMapNodes.Add(this);
		}
	}
	public void AddArmy(Army army) {
		Army sameIdArmy = armies.Find(a => a.Player.id == army.Player.id);
		if (sameIdArmy != null)
			sameIdArmy.Merge(army);
		else
			armies.Add(army);

		Events.InvokeArmyAddedEvent(this, army);
	}

	public void RemoveArmy(Army army) {
		armies.Remove(army);
		Events.InvokeArmyAddedEvent(this, army);
	}

	public void OnPointerClick(PointerEventData eventData) {
		if (Item.SelectedItem != null)
			Item.SelectedItem.Target = this;
	}

	internal void SetOwnerAndPublishEvent(int newOwnerId) {
		int oldOwnerId = ownerPlayerId;
		ownerPlayerId = newOwnerId;
		Events.InvokeOwnerChangedEvent(this, oldOwnerId, newOwnerId);
	}
}
