﻿public class PowerUp : MapNode {

	public PowerUp() : base() {
		Action1 = new UpgradeUnitAction(this);
		Action2 = new UpgradeUnitPassive(this);
	}

}
