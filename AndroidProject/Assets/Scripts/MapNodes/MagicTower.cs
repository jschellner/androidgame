﻿public class MagicTower : MapNode {

    public MagicTower(){
        Action1 = new CreateItem(this);
	}

    protected override void Awake() {
        base.Awake();
        magicImmune = true;
    }
}
