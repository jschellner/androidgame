﻿using System;
using System.Collections.Generic;
using Units;
using UnityEngine;
using UnityEngine.EventSystems;

public class ArmyMover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler {

	public static LinkedList<ArmyMover> SelectedPath { get; private set; } = new LinkedList<ArmyMover>();
	public static bool dragging = false;
	public static bool isMoving = false;

	public MapNode mapNode;
	public MapNodeUi ui;
	public LineRenderer LineRenderer { get; set; }
	private int selectionIndex = -1;

	private static void AddSelectedPathNode(ArmyMover selectedNode) {
		ArmyMover lastNode = GetLastSelectedPathNode();
		SelectedPath.AddLast(selectedNode);
		if (lastNode == null) {
			return;
		}

		lastNode.LineRenderer.SetPosition(0, lastNode.transform.position);
		lastNode.LineRenderer.SetPosition(1, selectedNode.transform.position);

		selectedNode.LineRenderer.enabled = true;
		CalculateLineRendererColor(selectedNode.LineRenderer);

		selectedNode.LineRenderer.SetPosition(0, selectedNode.transform.position);
	}

	private static void RemoveSelectedPathNode(ArmyMover selectedNode) {
		{
			List<ArmyMover> nodesToDelete = new List<ArmyMover>();
			LinkedListNode<ArmyMover> lastNode = SelectedPath.Last;
			while (!lastNode.Value.Equals(selectedNode)) {

				lastNode.Value.LineRenderer.enabled = false;
				lastNode.Value.selectionIndex = -1;
				SelectedPath.Remove(lastNode.Value);
				lastNode = SelectedPath.Last;
			}
		}
		{
			ArmyMover lastNode = GetLastSelectedPathNode();
			lastNode.LineRenderer.SetPosition(0, lastNode.transform.position);
			CalculateLineRendererColor(lastNode.LineRenderer);
		}
	}

	private static void ClearSelectedPathGraphics() {
		foreach (ArmyMover node in SelectedPath) {
			node.LineRenderer.enabled = false;
		}
	}

	private static void ClearSelectedPath() {
		foreach (ArmyMover node in SelectedPath)
			node.selectionIndex = -1;

		SelectedPath.Clear();
	}

	public static ArmyMover GetFirstSelectedNode() {
		return SelectedPath.First != null ? SelectedPath.First.Value : null;
	}


	public static ArmyMover GetLastSelectedPathNode() {
		return SelectedPath.Last != null ? SelectedPath.Last.Value : null;
	}
	private static void CalculateLineRendererColor(LineRenderer lineRenderer) {
		lineRenderer.startColor = new Color(0f, 1f, 0f, 0.1f);
		lineRenderer.endColor = new Color(0f, 1f, 0f, 0.1f);

		if (GameManager.instance.ActionPoints <= 0 || SelectedPath.Count > GameManager.instance.ActionPoints || NodeContainsOtherArmy(GetLastSelectedPathNode())) {
			lineRenderer.startColor = new Color(1f, 0f, 0f, 0.1f);
			lineRenderer.endColor = new Color(1f, 0f, 0f, 0.1f);
		}
	}

	private static bool NodeContainsOtherArmy(ArmyMover node) {
		if (node == null)
			return false;
		foreach (Army army in node.mapNode.armies)
			if (army.Player.id != GameManager.instance.ActivePlayerId)
				return true;
		return false;
	}

	protected void Awake() {
		LineRenderer = GetComponent<LineRenderer>();
		LineRenderer.enabled = false;
		mapNode = GetComponent<MapNode>();
		ui = GetComponent<MapNodeUi>();
	}

	protected void Start() {
		LineRenderer.SetPosition(0, transform.position);
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (isMoving)
			return;
		ClearSelectedPath();
		Item.SelectedItem = null;
		Army activePlayerArmy = mapNode.armies.Find(army => army.Player.id == GameManager.instance.ActivePlayerId);
		if (activePlayerArmy == null || GameManager.instance.ActionPoints <= 0 || activePlayerArmy.MovableUnits.Count == 0)
			return;
		LineRenderer.enabled = true;
		LineRenderer.SetPosition(0, transform.position);
		CalculateLineRendererColor(LineRenderer);
		LineRenderer.enabled = true;
		selectionIndex = 0;
		SelectedPath.AddFirst(this);
	}

	public void OnDrag(PointerEventData eventData) {
		ArmyMover lastSelectedNode = GetLastSelectedPathNode();
		if (lastSelectedNode != null) lastSelectedNode.LineRenderer.SetPosition(1, (Vector2)Camera.main.ScreenToWorldPoint(eventData.position));
	}

	public void OnEndDrag(PointerEventData eventData) {
		ClearSelectedPathGraphics();
	}

	public void OnDrop(PointerEventData eventData) {
		ArmyMover lastNode = GetLastSelectedPathNode();

		if (lastNode == null || !IsInSelectedPath())
			return;

		ArmyMover firstNode = GetFirstSelectedNode();
		if (firstNode.Equals(lastNode))
			return;

		Army army = firstNode.mapNode.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		if (army == null)
			return;

		if (army.MovableUnits.Count > 1)
			UnitSelection.instance.Show(this);
		else
			OnUnitSelectionSuccessful(army.MovableUnits);
	}

	public void OnUnitSelectionSuccessful(List<Unit> selectedUnits) {
		ArmyMover firstNode = GetFirstSelectedNode();
		Army army = firstNode.mapNode.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		Army movingArmy = army.Split(selectedUnits);
		SetArmyTargetsToSelectedPathInCorrectOrderAndCount(movingArmy);
		isMoving = true;
		ClearSelectedPathGraphics();
		ClearSelectedPath();
	}
	public void Clear() {
		ClearSelectedPathGraphics();
		ClearSelectedPath();
	}
	private void SetArmyTargetsToSelectedPathInCorrectOrderAndCount(Army movingArmy) {
		List<ArmyMover> temp = new List<ArmyMover>();
		foreach (ArmyMover mover in SelectedPath)
			temp.Add(mover);

		temp.RemoveAt(0);
		movingArmy.MovementTargets.Clear();
		temp.Sort((n1, n2) => n1.selectionIndex.CompareTo(n2.selectionIndex));
		for (int i = 0; i < temp.Count; i++)
			if (i < GameManager.instance.ActionPoints)
				movingArmy.MovementTargets.Add(temp[i].mapNode);

	}

	public void OnPointerEnter(PointerEventData eventData) {
		ArmyMover lastNode = GetLastSelectedPathNode();
		if (lastNode == null)
			return;

		if (SelectedPath.Contains(this)) {
			RemoveSelectedPathNode(this);
			return;
		}

		if (OtherCanMoveToThis(lastNode))
			AddSelectedPathNode(this);
	}

	public Boolean IsInSelectedPath() {
		return SelectedPath.Contains(this);
	}

	public bool OtherCanMoveToThis(ArmyMover other) {
		if (other == null || !other.mapNode.adjacentMapNodes.Contains(mapNode))
			return false;
		if (NodeContainsOtherArmy(other))
			return mapNode.ownerPlayerId == GameManager.instance.ActivePlayerId;
		return true;
	}

	public void OnPointerExit(PointerEventData eventData) {
		// TODO: hover effect?
	}

	public void OnPointerDown(PointerEventData eventData) {
		dragging = true;
	}

	public void OnPointerUp(PointerEventData eventData) {
		dragging = false;
	}
}
