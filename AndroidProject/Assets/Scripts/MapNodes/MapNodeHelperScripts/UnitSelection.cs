﻿using System.Collections.Generic;
using System.Linq;
using Units;
using UnityEngine;
using UnityEngine.UI;

public class UnitSelection : MonoBehaviour
{
    public static UnitSelection instance;

    private CanvasGroup canvasGroup;

    public Slider sliderNormal;
    public Text maxValNormal;
    public Text selectedValNormal;

    public Slider sliderUpgraded;
    public Text maxValUpgraded;
    public Text selectedValUpgraded;

    public Slider sliderGuard;
    public Text maxValGuard;
    public Text selectedValGuard;

    public Button okButton;
    private ArmyMover caller;

    // default: alle einheiten
    // bei halten von touch: unit selection

    void Awake()
    {
        instance = this;
        canvasGroup = GetComponent<CanvasGroup>();
        sliderNormal.onValueChanged.AddListener(delegate { OnSliderValueChangedNormal(sliderNormal.value); });
        sliderUpgraded.onValueChanged.AddListener(delegate { OnSliderValueChangedUpgraded(sliderUpgraded.value); });
        sliderGuard.onValueChanged.AddListener(delegate { OnSliderValueChangedGuard(sliderGuard.value); });
    }

    private void OnSliderValueChangedNormal(float val)
    {
        selectedValNormal.text = (int)val + "";
        UpdateOkButton();
    }

    private void OnSliderValueChangedUpgraded(float val)
    {
        selectedValUpgraded.text = (int)val + "";
        UpdateOkButton();
    }
    
    private void OnSliderValueChangedGuard(float val)
    {
        selectedValGuard.text = (int)val + "";
        UpdateOkButton();
    }

    private void UpdateOkButton()
    {
        okButton.interactable = sliderNormal.value != 0 || sliderUpgraded.value != 0 ||sliderGuard.value != 0;
    }
    public void Update()
    {
        UpdateSliderBasedOnNumberInput();
    }

    private void UpdateSliderBasedOnNumberInput()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            UpdateUnitsWithShortCut(1);

        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            UpdateUnitsWithShortCut(2);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            UpdateUnitsWithShortCut(3);

        }
        else if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            UpdateUnitsWithShortCut(4);

        }
        else if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            UpdateUnitsWithShortCut(5);

        }
        else if (Input.GetKeyUp(KeyCode.Alpha6))
        {
            UpdateUnitsWithShortCut(6);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha7))
        {
            UpdateUnitsWithShortCut(7);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            UpdateUnitsWithShortCut(8);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha9))
        {
            UpdateUnitsWithShortCut(9);
        }
    }

    private void UpdateUnitsWithShortCut(int value)
    {
        if (sliderNormal.maxValue > 0) sliderNormal.value = value;
        else sliderUpgraded.value = value;
    }

    public void Show(ArmyMover mover)
    {
        caller = mover;
        Army army = ArmyMover.GetFirstSelectedNode().mapNode.GetPlayerArmy(GameManager.instance.ActivePlayerId);
        int numNormalUnits = army.Units.OfType<Units.Infantry.Regular>().Count();
        int numUpgradedUnits = army.Units.OfType<Units.Infantry.Upgraded>().Count();
        int numGuardUnits = army.Units.OfType<Units.Infantry.Guard>().Count();

        sliderNormal.maxValue = numNormalUnits;
        sliderNormal.value = (float)numNormalUnits;

        sliderUpgraded.maxValue = numUpgradedUnits;
        sliderUpgraded.value = (float)numUpgradedUnits;
        
        sliderGuard.maxValue = numGuardUnits;
        sliderGuard.value = (float)numGuardUnits;

        maxValNormal.text = sliderNormal.maxValue + "";
        maxValUpgraded.text = sliderUpgraded.maxValue + "";
        maxValGuard.text = sliderGuard.maxValue + "";

        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    public void Hide(bool ok)
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;

        if (ok)
        {
            Army army = ArmyMover.GetFirstSelectedNode().mapNode.GetPlayerArmy(GameManager.instance.ActivePlayerId);
            List<Unit> selectedUnits = new List<Unit>();

            List<Unit> normalUnits = army.Units.OfType<Units.Infantry.Regular>().ToList<Unit>();
            for (int i = 0; i < (int)sliderNormal.value; i++)
                selectedUnits.Add(normalUnits[i]);

            List<Unit> upgradedUnits = army.Units.OfType<Units.Infantry.Upgraded>().ToList<Unit>();
            for (int i = 0; i < (int)sliderUpgraded.value; i++)
                selectedUnits.Add(upgradedUnits[i]); 
                
            List<Unit> guardUnits = army.Units.OfType<Units.Infantry.Guard>().ToList<Unit>();
            for (int i = 0; i < (int)sliderGuard.value; i++)
                selectedUnits.Add(guardUnits[i]);

            caller.OnUnitSelectionSuccessful(selectedUnits);
        }
        else
        {
            caller.Clear();
        }
        caller = null;
    }
}
