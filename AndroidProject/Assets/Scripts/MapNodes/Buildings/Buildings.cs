using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseBuilding {

	public List<BaseAction> Action { get; protected set; }
	public MapNode Location { get; set; } = null;
	public virtual int BuildingCosts { get; protected set; } = 1;
	public virtual bool CanUpgrade { get; protected set; } = false;
	public virtual bool IsPassiv { get; protected set; } = false;

	public abstract BaseBuilding Upgrade();

	public abstract void ApplyEffect();
}


public class Barracks : BaseBuilding {

	public override int BuildingCosts { get; protected set; } = 2;
	public override bool IsPassiv { get; protected set; } = false;
	public override bool CanUpgrade { get; protected set; } = false;

	public override void ApplyEffect() {
	}
	private static Barracks instance = new Barracks();

	public override BaseBuilding Upgrade() {
		return null;
	}

}