﻿public class RecruiteUnitAction : BaseAction {
	public RecruiteUnitAction(MapNode node) : base(node) {
		Label = "N+";
	}

	protected override void UpdateActiveState() {
		IsReady = CurrentPlayerOwns() && ContainsCurrentPlayerArmy() && CanAfford();
	}

	public override void Execute() {
		for (int i = 0; i < 10; i++) {
			GameManager.SpawnUnits(this.node, new Units.Infantry.Regular());
		}
		GameManager.DecreaseActionPointsAndFireEvent(this.ActionPointCosts);
	}

	public override BaseEffect Upgrade() {
		return null;
	}
}
