public class AttackAction : BaseAction {
	public AttackAction(MapNode node) : base(node) {
		Label = "A";
	}

	protected override void UpdateActiveState() {
		IsReady = CanAfford() && ContainsCurrentPlayerArmy() && ContainsEnemyArmy();
	}

	public override void Execute() {
		Events.InvokeAttackEvent(node);
	}

	public override BaseEffect Upgrade() {
		return null;
	}
}