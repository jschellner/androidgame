﻿public class ConquerAction : BaseAction {
	public ConquerAction(MapNode node) : base(node) {
		Label = "C";
	}

	protected override void UpdateActiveState() {
		Army playerArmy = node.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		IsReady = !CurrentPlayerOwns() && ContainsCurrentPlayerArmy() && CanAfford() && !ContainsEnemyArmy();
	}

	public override void Execute() {
		GameManager.instance.Conquer(node);
		GameManager.DecreaseActionPointsAndFireEvent(ActionPointCosts);
	}

	public override BaseEffect Upgrade() {
		return null;
	}
}
