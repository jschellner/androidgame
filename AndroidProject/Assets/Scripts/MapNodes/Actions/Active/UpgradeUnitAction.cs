﻿public class UpgradeUnitAction : BaseAction {
	public UpgradeUnitAction(MapNode node) : base(node) {
		Label = "U";
	}

	protected override void UpdateActiveState() {
		Army playerArmy = node.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		IsReady = CanAfford() && CurrentPlayerOwns() && ContainsCurrentPlayerArmy() && playerArmy.Units.Find(u => u.CanBeUpgraded) != null;
	}

	public override void Execute() {
		for (int i = 0; i < 10; i++) {
			GameManager.UpgradeUnit(node);
		}
		GameManager.DecreaseActionPointsAndFireEvent(ActionPointCosts);
	}

	public override BaseEffect Upgrade() {
		return null;
	}
}