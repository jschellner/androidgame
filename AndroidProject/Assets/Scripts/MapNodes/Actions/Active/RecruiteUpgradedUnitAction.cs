﻿public class RecruiteUpgradedUnitAction : BaseAction {
	public RecruiteUpgradedUnitAction(MapNode node) : base(node) {
		Label = "U++";
		ActionPointCosts = 4;
	}

	protected override void UpdateActiveState() {
		IsReady = CurrentPlayerOwns() && CanAfford();
	}

	public override void Execute() {
		GameManager.SpawnUnits(this.node, new Units.Infantry.Upgraded());
		GameManager.SpawnUnits(this.node, new Units.Infantry.Upgraded());
		GameManager.DecreaseActionPointsAndFireEvent(this.ActionPointCosts);

	}

	public override BaseEffect Upgrade() {
		return null;
	}
}
