﻿public class RecruiteGuardAction : BaseAction {
	public RecruiteGuardAction(MapNode node) : base(node) {
		Label = "N->G";
		ActionPointCosts = 1;
		Events.unitAddedEventListeners.Add((node, army, unit) => UpdateActiveState());
	}


	protected override void UpdateActiveState() {
		Army playerArmy = node.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		IsReady = CurrentPlayerOwns() && CanAfford() && ContainsCurrentPlayerArmy() && playerArmy.Units.FindAll(u => u is Units.Infantry.Regular).Count >= 10;
	}

	public override void Execute() {
		Army playerArmy = node.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		for (int i = 0; i < 10; i++) {
			GameManager.SpawnUnits(this.node, new Units.Infantry.Guard());
			GameManager.instance.RemoveUnit(playerArmy, playerArmy.GetRegularUnit());
		}
		GameManager.DecreaseActionPointsAndFireEvent(this.ActionPointCosts);
	}

	public override BaseEffect Upgrade() {
		return null;
	}
}
