
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseEffect {
	public virtual int ActionPointCosts { get; protected set; } = 1;

	public bool IsReady { get; set; }

	protected MapNode node;

	public BaseEffect(MapNode node) {
		this.node = node;
		Events.apChangedEventListeners.Add(UpdateActiveState);
	}

	protected abstract void UpdateActiveState();

	public abstract BaseEffect Upgrade();

	public abstract void Execute();
	public string Label { get; internal set; }

	public bool CurrentPlayerOwns() {
		return node.ownerPlayerId == GameManager.instance.ActivePlayerId;
	}

	public bool ContainsCurrentPlayerArmy() {
		return node.GetPlayerArmy(GameManager.instance.ActivePlayerId) != null;
	}

	public bool ContainsEnemyArmy() {
		return node.armies.FindAll(a => a.Player.id != GameManager.instance.ActivePlayerId).Count > 0;
	}

	public bool CanAfford() {
		return GameManager.instance.ActionPoints >= ActionPointCosts;
	}

	public bool NodeIsCastle() {
		return this.node is Castle;
	}
	public bool NodeIsWatchTower() {
		return this.node is WatchTower;
	}
}

public abstract class BasePassive : BaseEffect {
	public bool IsActive { get; set; } = true;
	public BasePassive(MapNode node) : base(node) {
	}

	protected override void UpdateActiveState() {
		IsReady = CurrentPlayerOwns() && IsActive;
	}

	public override BaseEffect Upgrade() {
		return null;
	}
	internal void ExecuteIfReadyAndActive() {
		if (IsReady && IsActive) Execute();
	}
}

public class TogglePassive : BaseEffect {
	public List<BasePassive> toggles;
	public int currentToggleIndex = 0;

	public TogglePassive(MapNode node, List<BasePassive> toggles) : base(node) {
		this.toggles = toggles;
		if (toggles.Count == 0)
			throw new Exception("Toggles can't be empty.");

		toggles.ForEach(x => x.IsActive = false);

		Label = toggles[0].Label;
		toggles[0].IsActive = true;


	}

	public override void Execute() {

		toggles[currentToggleIndex].IsActive = false;
		if (++currentToggleIndex >= toggles.Count)
			currentToggleIndex = 0;
		Label = toggles[currentToggleIndex].Label;
		toggles[currentToggleIndex].IsActive = true;
		GameManager.instance.InvokeUpdateUI();
	}

	public void ExecutePassive() {
		toggles[currentToggleIndex].Execute();
	}

	protected override void UpdateActiveState() {
		IsReady = CurrentPlayerOwns();
	}

	public override BaseEffect Upgrade() {
		return null;
	}
}

public abstract class BaseAction : BaseEffect {
	public BaseAction(MapNode node) : base(node) {
	}
}
