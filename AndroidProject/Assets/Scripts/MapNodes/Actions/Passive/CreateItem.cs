using UnityEngine;

public class CreateItem : BasePassive {

	public CreateItem(MapNode node) : base(node) {
		Events.nextRoundPhase1EventListeners.Add(ExecuteIfReadyAndActive);
	}

	public override void Execute() {
		Player activePlayer = GameManager.instance.ActivePlayer;
		if (activePlayer.items.Count < 3)
			activePlayer.AddItem(GameObject.Instantiate<Item1>(Prefabs.instance.item1Prefab, GameManager.instance.playerItems.transform));
	}
}
