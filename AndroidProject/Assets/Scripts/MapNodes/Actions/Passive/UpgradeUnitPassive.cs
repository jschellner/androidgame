﻿public class UpgradeUnitPassive : BasePassive {
	public UpgradeUnitPassive(MapNode node) : base(node) {
		Label = "U";
		Events.nextRoundPhase2EventListeners.Add(ExecuteIfReadyAndActive);
	}

	public override void Execute() {
		for (int i = 0; i < 20; i++) {
			GameManager.UpgradeUnit(GameManager.GetPlayer(node.ownerPlayerId).castle);
		}
	}
}