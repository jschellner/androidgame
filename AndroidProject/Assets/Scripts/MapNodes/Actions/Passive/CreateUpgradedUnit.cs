public class CreateUpgradedUnit : BasePassive {
	public CreateUpgradedUnit(MapNode node) : base(node) {
		Label = "U+";
		Events.nextRoundPhase1EventListeners.Add(ExecuteIfReadyAndActive);
	}

	public override void Execute() {
		for (int i = 0; i < 20; i++) {
			GameManager.SpawnUnits(node, new Units.Infantry.Upgraded());
		}
	}
}
