public class CreateGuard : BasePassive {

	public CreateGuard(MapNode node) : base(node) {
		Label = "G+";
		Events.nextRoundPhase1EventListeners.Add(ExecuteIfReadyAndActive);
	}

	public override void Execute() {
		for (int i = 0; i < 20; i++) {
			GameManager.SpawnUnits(node, new Units.Infantry.Guard());
		}
	}
}
