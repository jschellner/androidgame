using System;

public class CreateActionPoint : BasePassive {

	public CreateActionPoint(MapNode node) : base(node) {
		Label = "A+";
		Events.nextRoundPhase1EventListeners.Add(ExecuteIfReadyAndActive);
	}
	public override void Execute() {
		GameManager.instance.ActionPoints++;
	}
}
