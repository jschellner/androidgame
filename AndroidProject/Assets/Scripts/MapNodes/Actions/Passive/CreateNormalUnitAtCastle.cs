public class CreateNormalUnitAtCastle : BasePassive {

	public CreateNormalUnitAtCastle(MapNode node) : base(node) {
		Label = "N+C";
		Events.nextRoundPhase1EventListeners.Add(ExecuteIfReadyAndActive);
	}

	public override void Execute() {
		for (int i = 0; i < 10; i++) {
			GameManager.SpawnUnits(GameManager.GetPlayer(node.ownerPlayerId).castle, new Units.Infantry.Regular());
		}
	}
}
