﻿public class WatchTower : MapNode {

    public WatchTower():base(){
        Action1 = new RecruiteGuardAction(this);
        Action2 = new CreateActionPoint(this);
    }
}
