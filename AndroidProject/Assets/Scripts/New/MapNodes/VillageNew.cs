using UnityEngine;

public class VillageNew : MonoBehaviour {
	const float maxSpawnTime = 5f;

	public Villager villagerPrefab;
	public float spawnTime = 0f;
	public PlayerNew owner;
	public GameObject spawnPoint;


	protected void Update() {
		spawnTime += Time.deltaTime;
		if (spawnTime >= maxSpawnTime && owner != null) {
			spawnTime %= maxSpawnTime;
			spawnVillager();
		}
	}

	private void spawnVillager() {
		Villager villager = GameObject.Instantiate<Villager>(villagerPrefab, spawnPoint.transform.position, Quaternion.identity);
		villager.owner = owner;
	}
}
