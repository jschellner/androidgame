﻿using System.Collections.Generic;
using UnityEngine;

public class Player {
	public readonly int id;
	public readonly Color color;
	public readonly Castle castle;
	public List<Item> items = new List<Item>();
	public bool firstRound = true;
	private PlayerPortrait portrait;
	public PlayerPortrait Portrait {
		set {
			portrait = value;
			portrait.content.color = color;
		}
		get { return portrait; }
	}
	public PlayerUnitsUi unitsUi;

	public bool lost = false;

	public Player(int id, Color color, Castle castle) {
		this.id = id;
		this.color = color;
		this.castle = castle;
	}

	public void AddItem(Item item) {
		items.Add(item);
		OrderItems();
	}

	public void RemoveItem(Item item) {
		items.Remove(item);
		OrderItems();
	}

	private void OrderItems() {
		for (int i = 0; i < items.Count; i++) {
			Item item = items[i];
			float height = item.image.rectTransform.rect.height;
			item.image.transform.position = new Vector2(item.image.transform.position.x, 10 + 10 * i + i * height);
		}
	}

	public void Lose() {
		lost = true;
		Portrait.content.color = Color.black;
		GameManager.CheckWinConditions();
		foreach (Army army in GameObject.FindObjectsOfType<Army>()) {
			if (army.Player == this) {
				while (army.Units.Count > 0) {
					GameManager.instance.RemoveUnit(army, army.Units[0]);
				}
			}
		}
	}
}
