﻿using System;
using System.Collections.Generic;
using System.Linq;
using Units;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUnitsUi : MonoBehaviour
{
    private Army army;
    public Army Army
    {
        set
        {
            army = value;
            foreach (Transform child in transform)
                GameObject.Destroy(child.gameObject);
            if (army == null)
                return;

            Dictionary<string, List<Unit>> unitTypeList = army.GetGroupedUnits();
            foreach (var pair in unitTypeList)
            {
                Text t = GameObject.Instantiate(Prefabs.instance.playerUnitNumberPrefab, transform);
                t.text = pair.Key + ": " + pair.Value.Count();
            }
        }
        get
        {
            return army;
        }
    }
}
