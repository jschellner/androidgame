﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerPortrait : MonoBehaviour
{
    public Image frame;
    public Image content;

    private void Awake() {
        frame = GetComponent<Image>();
        content = transform.GetChild(0).GetComponent<Image>();
    }

}
