using UnityEngine;

public interface IArmyFightHandler{

	public BattleLog BattleLog { get; set; }
	public void Fight(Army attackers, Army defenders);
}
