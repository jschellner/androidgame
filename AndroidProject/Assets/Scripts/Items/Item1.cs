﻿using System.Collections.Generic;

public class Item1 : Item {
	protected override string RunEffect() {

		Player activePlayer = GameManager.instance.ActivePlayer;
		bool activePlayerHasAtLeastOneUnitAtAtLeastOneMagicTowers =
								GameManager
										.GetMapNodes()
										.FindAll(node => node is MagicTower)
										.FindAll(magicTower => magicTower.GetPlayerArmy(activePlayer.id).Units.Count > 0)
										.Count > 0;

		if (!activePlayerHasAtLeastOneUnitAtAtLeastOneMagicTowers)
			return "Player has to have at least one unit at at least one magic tower!";

		if (Target.magicImmune)
			return "This spot is magic immune";

		Army army = Target.armies.Find(a => a.Player != GameManager.instance.ActivePlayer);

		List<Units.Unit> upgradedUnits = army.Units.FindAll(u => u is Units.Infantry.Upgraded);
		if (upgradedUnits.Count > 0) {
			for (int i = 0; i < 10 && i < upgradedUnits.Count; i++) {
				GameManager.instance.RemoveUnit(army, upgradedUnits[i]);
			}
		} else {
			List<Units.Unit> regularUnits = army.Units.FindAll(u => u is Units.Infantry.Regular);
			for (int i = 0; i < 20 && i < regularUnits.Count; i++) {
				GameManager.instance.RemoveUnit(army, regularUnits[i]);
			}
		}
		return null;
	}
}
