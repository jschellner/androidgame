﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IPointerClickHandler {
	public int useCost = 2;

	private static Item selectedItem;
	public static Item SelectedItem {
		set {
			if (selectedItem != null)
				selectedItem.Unhighlight();
			selectedItem = value;
			if (selectedItem != null)
				selectedItem.Highlight();
		}
		get { return selectedItem; }
	}

	public Image image;
	private MapNode target;
	public MapNode Target {
		set {
			target = value;
			RunEffectAndDestroy();
			target = null;
		}
		get { return target; }
	}

	protected virtual void Awake() {
		image = GetComponent<Image>();
	}

	public void OnPointerClick(PointerEventData eventData) {
		if (GameManager.instance.ActionPoints < calculateUseCost())
			return;
		SelectedItem = SelectedItem == this ? null : this;
	}

	protected virtual string RunEffect() {
		return null;
	}

	protected void RunEffectAndDestroy() {
		string result = RunEffect();
		if (result != null) {
			print(result);
			return;
		}
		int useCost = calculateUseCost();
		GameManager.instance.ActivePlayer.RemoveItem(this);
		GameObject.Destroy(this.gameObject);
		if (useCost > 0) {
			GameManager.DecreaseActionPointsAndFireEvent(useCost);
		}
	}

	private int calculateUseCost() {
		if (GameManager.instance.ActivePlayer.items.Count == 3) {
			return 0;
		} else if (GameManager.instance.ActivePlayer.items.Count == 2) {
			return 1;
		} else if (GameManager.instance.ActivePlayer.items.Count == 1) {
			return 2;
		} else if (GameManager.instance.ActivePlayer.items.Count == 0) {
			return 3;
		} else {
			return 999;
		}
	}

	private void Highlight() {
		image.color = Color.red;
	}

	private void Unhighlight() {
		image.color = Color.white;
	}
}
