﻿using UnityEngine;
using UnityEngine.UI;

public class Prefabs : MonoBehaviour {
    public static Prefabs instance;

    public Army armyPrefab;
    public Castle castlePrefab;
    public Village villagePrefab;
    public WatchTower watchTowerPrefab;
    public PowerUp powerUpPrefab;
    public MagicTower magicTowerPrefab;
    public Waypoint waypointPrefab;

    public Item1 item1Prefab;
    public LineRenderer linePointPrefab;
    public LineRenderer armyMoverConnectionPrefab;

    public PlayerPortrait playerPortraitPrefab;
    public PlayerUnitsUi playerUnitsUiPrefab;
    public Text playerUnitNumberPrefab;

    public MapCreationNode mapCreationNodePrefab;

    private void Awake() {
        instance = this;
    }
}
