﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour {
    private class NodeHolder {
        public MapNode mapNode;
        public NodeHolder predecessor;

        public NodeHolder(MapNode mapNode, NodeHolder predecessor) {
            this.mapNode = mapNode;
            this.predecessor = predecessor;
        }
    }

    public static List<MapNode> GetShortestPath(MapNode startingNode, MapNode targetNode) {
        if (startingNode == null || targetNode == null)
            throw new System.Exception("Start and target can not be null");

        List<MapNode> visitedNodes = new List<MapNode>();
        List<NodeHolder> nodesToVisit = new List<NodeHolder>();

        NodeHolder current = new NodeHolder(startingNode, null);
        nodesToVisit.Add(current);

        bool reachedTarget = false;
        while (nodesToVisit.Count != 0) {
            current = nodesToVisit[0];
            if (current.mapNode == targetNode) {
                reachedTarget = true;
                break;
            }

            visitedNodes.Add(current.mapNode);
            nodesToVisit.RemoveAt(0);

            current.mapNode.adjacentMapNodes.FindAll(adjN => !visitedNodes.Contains(adjN)).ForEach(adjN => nodesToVisit.Add(new NodeHolder(adjN, current)));
        }

        if (!reachedTarget)
            return null;

        List<MapNode> path = new List<MapNode>();
        MapNode c = current.mapNode;
        path.Add(c);
        while (current.predecessor != null && current.predecessor.mapNode != startingNode) {
            path.Insert(0, current.predecessor.mapNode);
            current = current.predecessor;
        }

        return path;
    }
}
