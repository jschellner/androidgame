﻿using System.Collections.Generic;
using UnityEngine;
using static MapEditor;

public class MapCreator {
	public static List<Castle> CreateMap() {
		List<Castle> castles = new List<Castle>();

		Castle c1 = CreateNodeAtPosition(Prefabs.instance.castlePrefab, -3, 0);
		Castle c2 = CreateNodeAtPosition(Prefabs.instance.castlePrefab, 3, 0);

		WatchTower w1l = CreateNodeAtPosition(Prefabs.instance.watchTowerPrefab, -2, -1);
		WatchTower w1r = CreateNodeAtPosition(Prefabs.instance.watchTowerPrefab, -2, 1);
		c1.AddAdjacentNodes(w1l, w1r);

		WatchTower w2l = CreateNodeAtPosition(Prefabs.instance.watchTowerPrefab, 2, -1);
		WatchTower w2r = CreateNodeAtPosition(Prefabs.instance.watchTowerPrefab, 2, 1);
		c2.AddAdjacentNodes(w2l, w2r);

		Village v1l = CreateNodeAtPosition(Prefabs.instance.villagePrefab, -1, -2);
		Village v1c = CreateNodeAtPosition(Prefabs.instance.villagePrefab, -1, 0);
		Village v1r = CreateNodeAtPosition(Prefabs.instance.villagePrefab, -1, 2);
		w1l.AddAdjacentNodes(v1l, v1c);
		w1r.AddAdjacentNodes(v1c, v1r);
		v1c.AddAdjacentNodes(v1l, v1r);

		Village v2l = CreateNodeAtPosition(Prefabs.instance.villagePrefab, 1, -2);
		Village v2c = CreateNodeAtPosition(Prefabs.instance.villagePrefab, 1, 0);
		Village v2r = CreateNodeAtPosition(Prefabs.instance.villagePrefab, 1, 2);
		w2l.AddAdjacentNodes(v2l, v2c);
		w2r.AddAdjacentNodes(v2c, v2r);
		v2c.AddAdjacentNodes(v2l, v2r);

		PowerUp pl = CreateNodeAtPosition(Prefabs.instance.powerUpPrefab, 0, -1);
		PowerUp pr = CreateNodeAtPosition(Prefabs.instance.powerUpPrefab, 0, 1);
		pl.AddAdjacentNodes(v1l, v1c, v2l, v2c);
		pr.AddAdjacentNodes(v1c, v1r, v2c, v2r);

		MagicTower mtl = CreateNodeAtPosition(Prefabs.instance.magicTowerPrefab, 0, -2);
		MagicTower mtr = CreateNodeAtPosition(Prefabs.instance.magicTowerPrefab, 0, 2);
		mtl.AddAdjacentNodes(pl);
		mtr.AddAdjacentNodes(pr);

		castles.Add(c1);
		castles.Add(c2);
		return castles;
	}

	private static T CreateNodeAtPosition<T>(T prefab, float x, float y, int id = 0) where T : MapNode {
		T instance = Object.Instantiate(prefab, GameManager.instance.mapNodes.transform);
		instance.transform.position = new Vector2(x, y);
		instance.id = id;
		return instance;
	}

	public static List<Castle> LoadFromFile(string filepath) {
		MapData mapData = JsonUtility.FromJson<MapData>(System.IO.File.ReadAllText(filepath));

		Dictionary<string, MapNode> prefabMapping = new Dictionary<string, MapNode>();
		prefabMapping.Add("castle", Prefabs.instance.castlePrefab);
		prefabMapping.Add("watchTower", Prefabs.instance.watchTowerPrefab);
		prefabMapping.Add("village", Prefabs.instance.villagePrefab);
		prefabMapping.Add("powerUp", Prefabs.instance.powerUpPrefab);
		prefabMapping.Add("magicTower", Prefabs.instance.magicTowerPrefab);
		prefabMapping.Add("waypoint", Prefabs.instance.waypointPrefab);

		Dictionary<int, MapNode> nodeMap = new Dictionary<int, MapNode>();
		mapData.nodes.ForEach(n => {
			MapNode node = CreateNodeAtPosition(prefabMapping[n.type], n.position.x, n.position.y, n.id);
			nodeMap.Add(node.id, node);
		});
		List<Castle> castles = new List<Castle>();
		castles.AddRange(GameObject.FindObjectsOfType<Castle>());
		castles.Sort((Castle c1, Castle c2) => c1.transform.position.x.CompareTo(c2.transform.position.x));

		mapData.nodes.ForEach(n => {
			MapNode node = nodeMap[n.id];
			n.adjacentNodes.ForEach(adjNodeId => {
				try {
					node.AddAdjacentNodes(nodeMap[adjNodeId]);
				} catch (KeyNotFoundException e) {
					System.Console.WriteLine("adjNode: " + adjNodeId);
					System.Console.WriteLine("node: " + node);
					// throw e;
				}
			});
		});

		return castles;
	}
}
