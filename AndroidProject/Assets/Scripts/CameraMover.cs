﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    public CameraMover instance;

    //public float speed = 0.1F;

    //void Update() {
    //    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
    //        Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
    //        transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);
    //    }
    //}

    private Vector3 dragOrigin;
    private Vector3 clickOrigin = Vector3.zero;
    private Vector3 basePos = Vector3.zero;

    private void Awake() {
        instance = this;
    }

    void Update() {
        if (ArmyMover.dragging)
            return;

        if (!Input.GetMouseButton(0)) {
            clickOrigin = Vector3.zero;
            return;
        }
        if (clickOrigin == Vector3.zero) {
            clickOrigin = Input.mousePosition;
            basePos = transform.position;
        }
        dragOrigin = Input.mousePosition;

        float x = Mathf.Clamp(basePos.x + ((clickOrigin.x - dragOrigin.x) * .01f), -1, 1);
        float y = Mathf.Clamp(basePos.y + ((clickOrigin.y - dragOrigin.y) * .01f), -1, 1);
        transform.position = new Vector3(x, y, transform.position.z);
    }
}
