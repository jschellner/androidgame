using UnityEngine;

public class CoinFlipper : MonoBehaviour {
	private SpriteRenderer spriteRenderer;
	private new Animation animation;


	protected void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		animation = GetComponent<Animation>();
		Events.nextRoundPhase1EventListeners.Add(StartAnimation);
	}

	public void StartAnimation() {
		animation.Play();
	}

	public void InitColor(){
		spriteRenderer.color = GameManager.GetPreviousPlayer().color;
	}

	public void SwitchColor(){
		spriteRenderer.color = GameManager.instance.ActivePlayer.color;
	}
}
