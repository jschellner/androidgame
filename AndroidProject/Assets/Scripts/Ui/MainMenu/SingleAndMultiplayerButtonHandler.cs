using System.Collections.Generic;
using UnityEngine;

public class SingleAndMultiplayerButtonHandler : MonoBehaviour {
	public List<KnightAnimator> knights;

	public void changeState(string state) {
		foreach (KnightAnimator knight in knights)
			knight.ChangeAnimation(state);
	}
}
