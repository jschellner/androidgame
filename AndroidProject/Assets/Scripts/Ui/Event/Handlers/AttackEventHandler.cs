public class AttackEventHandler {

	static internal void HandleEvent(MapNode node) {
		Army playerArmy = node.GetPlayerArmy(GameManager.instance.ActivePlayerId);
		Army opposingArmy = node.armies.Find(a => a.Player.id != GameManager.instance.ActivePlayerId);

		playerArmy.AttackArmy(opposingArmy);
		GameManager.DecreaseActionPointsAndFireEvent(1);
	}

}
