using System;
using System.Collections.Generic;
using Units;
using UnityEngine;
using UnityEngine.UI;

public class UiEventHandler {

    private static readonly Color nonOwnerColor = Color.gray;

    internal static void HandleArmyAddedEvent(MapNode node, Army army) {
        UpdateText(node);
        UpdateArmyPositions(node);
    }

    internal static void HandleArmyRemovedEvent(MapNode node, Army army) {
        UpdateText(node);
        UpdateArmyPositions(node);
    }

    internal static void HandleUnitAddedEvent(MapNode node, Army army, Unit unit) {
        UpdateText(node);
    }

    internal static void HandleUnitRemovedEvent(MapNode node, Army army, Unit unit) {
        UpdateText(node);
        UpdateArmyPositions(node);
    }

    internal static void HandleUpgradeEvent(MapNode node) {
        UpdateText(node);
    }

    internal static void UpdateText(MapNode node) {
        if (node == null)
            return;

        Text textP1Units = null;
        Text textP2Units = null;
        foreach (Text t in node.gameObject.GetComponentsInChildren<Text>()) {
            switch (t.name) {
                case "TextP1Units":
                    textP1Units = t;
                    break;
                case "TextP2Units":
                    textP2Units = t;
                    break;
            }
        }

        Army army1 = node.GetPlayerArmy(1);
        Army army2 = node.GetPlayerArmy(2);

        textP1Units.text = null;
        textP2Units.text = null;
        if (army1 != null)
            textP1Units.text = CreateArmyStrings(army1);
        if (army2 != null)
            textP2Units.text = CreateArmyStrings(army2);
    }

    internal static string CreateArmyStrings(Army army) {
        String result = "";
        Dictionary<string, List<Unit>> unitTypeList = army.GetGroupedUnits();
        foreach (var pair in unitTypeList) {
            result = result == null ? pair.Key + ": " : result + "\n" + pair.Key + ": ";
            result += pair.Value.Count;
        }
        return result;
    }

    internal static void UpdateArmyPositions(MapNode node) {
        if (node.armies.Count == 0)
            return;
        if (node.armies.Count == 1) {
            node.armies[0].transform.position = node.transform.position;
            return;
        }
        node.armies.Sort((army1, army2) => army1.Player.id.CompareTo(army2.Player.id));
        node.armies[0].transform.position = new Vector2(node.transform.position.x - 0.1f, node.transform.position.y);
        node.armies[1].transform.position = new Vector2(node.transform.position.x + 0.1f, node.transform.position.y);
    }

    internal static void HandleOwnerChangedEvent(MapNode node, int oldOwnerId, int newOwnerId) {
        if (newOwnerId != 0)
            node.spriteRenderer.color = GameManager.GetPlayer(newOwnerId).color;
        else
            node.spriteRenderer.color = nonOwnerColor;
    }
}
