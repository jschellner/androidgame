using System;
using System.Collections.Generic;
using Units;

public static class Events {
	public static List<Action> apChangedEventListeners = new List<Action>();
	public static List<Action> nextRoundPhase1EventListeners = new List<Action>();
	public static List<Action> nextRoundPhase2EventListeners = new List<Action>();
	public static List<Action<MapNode>> attackEventListeners = new List<Action<MapNode>>();
	public static List<Action<MapNode>> conquerEventListeners = new List<Action<MapNode>>();
	public static List<Action<MapNode>> upgradeEventListeners = new List<Action<MapNode>>();
	public static List<Action<MapNode, Army>> armyAddedEventListeners = new List<Action<MapNode, Army>>();
	public static List<Action<MapNode, Army>> armyRemovedEventListeners = new List<Action<MapNode, Army>>();
	public static List<Action<MapNode, Army, Unit>> unitAddedEventListeners = new List<Action<MapNode, Army, Unit>>();
	public static List<Action<MapNode, Army, Unit>> unitRemovedEventListeners = new List<Action<MapNode, Army, Unit>>();
	public static List<Action<MapNode, int, int>> ownerChangedEventListeners = new List<Action<MapNode, int, int>>();
	public static List<Action> updateUiEventListeners = new List<Action>();

	internal static void SetupDefaultEventHandlers() {
		attackEventListeners.Add(AttackEventHandler.HandleEvent);

		conquerEventListeners.Add(ConquerEventHandler.HandleEvent);

		upgradeEventListeners.Add(UpgradeEventHandler.HandleEvent);

		upgradeEventListeners.Add(UiEventHandler.HandleUpgradeEvent);
		armyAddedEventListeners.Add(UiEventHandler.HandleArmyAddedEvent);
		armyRemovedEventListeners.Add(UiEventHandler.HandleArmyAddedEvent);
		unitAddedEventListeners.Add(UiEventHandler.HandleUnitAddedEvent);
		unitRemovedEventListeners.Add(UiEventHandler.HandleUnitAddedEvent);
		ownerChangedEventListeners.Add(UiEventHandler.HandleOwnerChangedEvent);
	}

	internal static void InvokeApChangedEvent() {
		apChangedEventListeners.ForEach(l => l.Invoke());
		InvokeUpdateUiEvent();
	}

	internal static void InvokeNextRoundPhase1Event() {
		nextRoundPhase1EventListeners.ForEach(l => l.Invoke());
		InvokeNextRoundPhase2Event();
	}
	
	private static void InvokeNextRoundPhase2Event() {
		nextRoundPhase2EventListeners.ForEach(l => l.Invoke());
		InvokeUpdateUiEvent();
	}

	internal static void InvokeAttackEvent(MapNode node) {
		attackEventListeners.ForEach(l => l.Invoke(node));
	}

	internal static void InvokeConquerEvent(MapNode node) {
		conquerEventListeners.ForEach(l => l.Invoke(node));
	}

	internal static void InvokeUpgradeEvent(MapNode node) {
		upgradeEventListeners.ForEach(l => l.Invoke(node));
		InvokeUpdateUiEvent();
	}

	internal static void InvokeArmyAddedEvent(MapNode node, Army army) {
		armyAddedEventListeners.ForEach(l => l.Invoke(node, army));
	}

	internal static void InvokeArmyRemovedEvent(MapNode node, Army army) {
		armyAddedEventListeners.ForEach(l => l.Invoke(node, army));
	}

	internal static void InvokeUnitAddedEvent(MapNode node, Army army, Unit unit) {
		unitAddedEventListeners.ForEach(l => l.Invoke(node, army, unit));
		InvokeUpdateUiEvent();
	}

	internal static void InvokeUnitRemovedEvent(MapNode node, Army army, Unit unit) {
		unitAddedEventListeners.ForEach(l => l.Invoke(node, army, unit));
	}

	internal static void InvokeOwnerChangedEvent(MapNode node, int oldOwnerId, int newOwnerId) {
		ownerChangedEventListeners.ForEach(l => l.Invoke(node, oldOwnerId, newOwnerId));
	}

	internal static void InvokeUpdateUiEvent() {
		updateUiEventListeners.ForEach(l => l.Invoke());
	}
}
