using UnityEngine;
using UnityEngine.UI;

public class MouseInfo : MonoBehaviour {
	public static MouseInfo instance;
	private Text text;

	protected void Awake() {
		instance = this;
		text = GetComponentInChildren<Text>();
	}

	protected void Update() {
		Vector3 v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		transform.position = new Vector2(v.x + 0.3f, v.y);
	}

	public void SetText(string text) {
		this.text.text = text;
	}
}
