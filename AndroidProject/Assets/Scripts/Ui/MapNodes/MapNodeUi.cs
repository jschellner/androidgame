﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MapNodeUi : MonoBehaviour, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {
	private MapNode node;

	public Button attackButton;
	public Button conquerButton;
	public Button action1Button;
	public Button action2Button;

	protected void Awake() {
		node = GetComponent<MapNode>();

		PrepareActionButton(attackButton, node.AttackAction);
		PrepareActionButton(conquerButton, node.ConquerAction);
		PrepareActionButton(action1Button, node.Action1);
		PrepareActionButton(action2Button, node.Action2);

		Events.updateUiEventListeners.Add(UpdateUi);
	}

	private void UpdateUi() {
		UpdateButtonInformation(attackButton,node.AttackAction);
		UpdateButtonInformation(conquerButton, node.ConquerAction);
		UpdateButtonInformation(action1Button, node.Action1);
		UpdateButtonInformation(action2Button, node.Action2);
	}

	private void UpdateButtonInformation(Button b, BaseEffect e) {
		if (e != null) {
			b.GetComponentInChildren<Text>().text = e.Label;
			b.gameObject.SetActive(e.IsReady);
		} else
			b.gameObject.SetActive(false);
	}

	private void PrepareActionButton(Button b, BaseEffect a) {
		if (a == null)
			return;
		if (!(a is BasePassive))
			b.onClick.AddListener(a.Execute);

		b.GetComponentInChildren<Text>().text = a.Label;
	}

	public void OnPointerUp(PointerEventData eventData) {
		GameManager.instance.SelectedMapNode = node;
	}

	public void OnPointerEnter(PointerEventData eventData) {
		MouseInfo.instance.SetText(node.GetType() + "");
	}

	public void OnPointerExit(PointerEventData eventData) {
		MouseInfo.instance.SetText(null);
	}
}
