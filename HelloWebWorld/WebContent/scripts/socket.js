class Socket{
	constructor(){
// this.socket = new WebSocket("wss://localhost:30033");
		this.socket = new WebSocket('ws://games.owbgames.de:30033');
		let s = this.socket;

		this.socket.onopen = function(e) {
		};

		this.socket.onmessage = function(event) {
			//console.log(event.data);
			if(event.data.startsWith("connect:")){
				let name = event.data.substring("connect:".length);
				if(name == player.name)
					return;
				otherPlayers.push(new OtherPlayer(name));
			}
			if(event.data.startsWith("move:")){
				let split = event.data.split(":");
				if(player.name == split[1])
					return;
				let temp = otherPlayers.find(p => p.name == split[1]);
				let otherPlayer = temp != null ? temp : new OtherPlayer(split[1]); 
				if(temp == null)
					otherPlayers.push(otherPlayer);
				otherPlayer.x = parseFloat(split[2]);
				otherPlayer.y = parseFloat(split[3]);
			}
		};

		this.socket.onclose = function(event) {
			if (event.wasClean) {
				alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
			} else {
				// e.g. server process killed or network down
				// event.code is usually 1006 in this case
				alert('[close] Connection died');
			}
		};

		this.socket.onerror = function(error) {
			alert(`[error] ${error.message}`);
		};
	}
	
	send(msg){
		this.socket.send(msg);
	}
}