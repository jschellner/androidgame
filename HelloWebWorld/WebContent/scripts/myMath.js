function rectContainsPoint(rect, x, y) {
	return rect.x <= x && x <= rect.x + rect.width &&
		rect.y <= y && y <= rect.y + rect.height;
}

function rectContainsPoint2(rectX, rectY, rectWidth, rectHeight, x, y) {
	return rectX <= x && x <= rectX + rectWidth &&
		rectY <= y && y <= rectY + rectHeight;
}