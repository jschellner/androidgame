let inventoryMargin = 8;
let itemSize = 20;
let itemsPerRow = 5;

class Inventory {
	constructor() {
		this.items = [];
		this.cash = 0;
		this.hidden = true;
		window.addEventListener('keyup', e => { if (e.key == 'i') this.hidden = !this.hidden; });
		this.x = 400;
		this.y = 200;
		this.width = 400;
		this.height = 600;
	}

	draw() {
		if (this.hidden)
			return;
		canvas.canvasContext.fillStyle = "orange";
		canvas.canvasContext.fillRect(this.x, this.y, this.width, this.height);

		for (let i = 0; i < this.items.length; i++) {
			let row = Math.floor(i / itemsPerRow);
			let col = i % itemsPerRow;
			this.items[i].draw(this.x + col * (itemSize + inventoryMargin), this.y + row * (itemSize + inventoryMargin));
		}
	}

	onMousePressed(e) {
		if (this.hidden)
			return;
		if (!this.contains(e.x, e.y))
			return;
		for (let i = 0; i < this.items.length; i++) {
			let row = Math.floor(i / itemsPerRow);
			let col = i % itemsPerRow;
			let itemX = this.x + col * (itemSize + inventoryMargin);
			let itemY = this.y + row * (itemSize + inventoryMargin);
			if (rectContainsPoint2(itemX, itemY, itemSize, itemSize, e.x, e.y)) {
				this.items[i].activate();
				break;
			}
		}
	}

	contains(x, y) {
		return rectContainsPoint(this, x, y);
	}

}