async function restTest() {
	const response = await fetch('https://google.com', {
		method: 'GET',
		// body: "asd",
		// headers: {
		//	'Content-Type': 'application/json'
		// }
	});
	// const myJson = await response.json();
	// console.log(myJson);
}

restTest();

function resolveAfter2Seconds() {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve('resolved');
		}, 2000);
	});
}

async function asyncTest() {
	console.log("calling");
	console.log(await resolveAfter2Seconds());
	console.log("called");
}

asyncTest();
console.log("blub");