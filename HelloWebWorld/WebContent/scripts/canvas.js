class Canvas {
	constructor(canvas) {
		this.canvas = canvas;
		this.canvasContext = canvas.getContext("2d");
		this.resize();
		window.addEventListener('resize', () => this.resize());
		this.oldTimeStamp = 0;
		this.deltaTime = 0;
	}

	animate(timeStamp) {
		this.deltaTime = timeStamp - this.oldTimeStamp;
		this.secondsPassed = this.deltaTime / 1000;
		this.oldTimeStamp = timeStamp;
		this.fps = Math.round(1 / this.secondsPassed);
		if (isNaN(this.deltaTime)) {
			requestAnimationFrame(timeStamp => this.animate(timeStamp));
			return;
		}

		this.canvasContext.clearRect(0, 0, innerWidth, innerHeight);

		stageLoader.currentStage.draw();

		for(let p of otherPlayers)
			p.draw();
		
		player.update();
		player.draw();
		
		this.canvasContext.font = "20px Arial";
		this.canvasContext.fillStyle = "black";
		this.canvasContext.fillText("FPS: " + this.fps, 2, 30);

		requestAnimationFrame(timeStamp => this.animate(timeStamp));
	}

	resize() {
		this.canvas.width = innerWidth;
		this.canvas.height = innerHeight;
	}
}