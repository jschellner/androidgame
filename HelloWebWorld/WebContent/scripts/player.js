let statsMargin = 8;

class Player {
	constructor() {
		this.name = "anonymous";
		this.x = 100;
		this.y = 100;
		this.width = 64;
		this.height = 64;
		this.moveSpeed = 0.2;
		this.sprite = null;
		this.isStatic = false;
		this.blocksMovement = false;
		this.hp = 1;
		this.maxHp = 10;
		this.inventory = new Inventory();
		for (let i = 0; i < 6; i++)
			this.inventory.items.push(createItem("apple", 1));
		this.inventory.items.push(createItem("iron_ore", 1));
		
		this.oldX = 0;
		this.oldY = 0;
	}

	update() {
		if (this.sprite == null)
			this.sprite = images.get("villager");
		let deltaMoveSpeed = this.moveSpeed * canvas.deltaTime;
		if (pressedKeys.has('w') && !this.collidesWithMovementBlocker(this.x, this.y - deltaMoveSpeed) && this.isInBounds(this.x, this.y - deltaMoveSpeed))
			this.y -= deltaMoveSpeed;
		if (pressedKeys.has('s') && !this.collidesWithMovementBlocker(this.x, this.y + deltaMoveSpeed) && this.isInBounds(this.x, this.y + deltaMoveSpeed))
			this.y += deltaMoveSpeed;
		if (pressedKeys.has('a')) {
			this.sprite = images.get("villagerFlipped");
			if (!this.collidesWithMovementBlocker(this.x - deltaMoveSpeed, this.y) && this.isInBounds(this.x - deltaMoveSpeed, this.y))
				this.x -= deltaMoveSpeed;
		}
		if (pressedKeys.has('d')) {
			this.sprite = images.get("villager");
			if (!this.collidesWithMovementBlocker(this.x + deltaMoveSpeed, this.y) && this.isInBounds(this.x + deltaMoveSpeed, this.y))
				this.x += deltaMoveSpeed;
		}
		
		if(this.x == this.oldX && this.y == this.oldY)
			return;
		socket.send("move:" + player.name +":"+this.x + ":"+this.y);
		this.oldX = this.x;
		this.oldY = this.y;
	}

	draw() {
		canvas.canvasContext.drawImage(this.sprite, this.x, this.y, this.width, this.height);
		this.inventory.draw();
		
		canvas.canvasContext.font = "15px Arial";
		canvas.canvasContext.fillStyle = "green";
		let textMeasure = canvas.canvasContext.measureText(this.name);
		let x = this.x + this.width / 2 - textMeasure.width / 2;
		canvas.canvasContext.fillText(this.name, x, this.y + this.height + textMeasure.actualBoundingBoxAscent + textMeasure.actualBoundingBoxDescent + statsMargin);

		canvas.canvasContext.font = "20px Arial";
		canvas.canvasContext.fillStyle = "red";
		let hpText = "HP: " + this.hp + "/" + this.maxHp;
		textMeasure = canvas.canvasContext.measureText(hpText);
		canvas.canvasContext.fillText(hpText, innerWidth - textMeasure.width - statsMargin, textMeasure.actualBoundingBoxAscent + textMeasure.actualBoundingBoxDescent + statsMargin);
	}
	
	collidesWithMovementBlocker(x, y, fireEvents = true) {
		let collision = false;
		for (let gameObject of gameObjects)
			if (gameObject != this && gameObject.collides(x, y, this.width, this.height, fireEvents) && gameObject.blocksMovement)
				collision = true;
		return collision;
	}

	isInBounds(x, y) {
		return x > 0 && y > 0 && x + this.width < innerWidth && y + this.height < innerHeight;
	}

	heal(value) {
		this.hp = Math.min(this.maxHp, this.hp + value);
	}

}
