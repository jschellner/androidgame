class OtherPlayer{
	constructor(name){
		this.name = name;
		this.x = 0;
		this.y = 0;
		this.width = 20;
		this.height = 20;
		this.sprite = null;
	}
	
	draw(){
		if (this.sprite == null)
			this.sprite = images.get("villager");
		
		canvas.canvasContext.drawImage(this.sprite, this.x, this.y, this.width, this.height);
		
		canvas.canvasContext.font = "15px Arial";
		canvas.canvasContext.fillStyle = "orange";
		let textMeasure = canvas.canvasContext.measureText(this.name);
		let x = this.x + this.width / 2 - textMeasure.width / 2;
		canvas.canvasContext.fillText(this.name, x, this.y + this.height + textMeasure.actualBoundingBoxAscent + textMeasure.actualBoundingBoxDescent + statsMargin);
	}
}