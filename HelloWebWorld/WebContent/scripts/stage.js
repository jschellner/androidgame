class Stage {
	constructor() {
		this.northGate = null;
		this.eastGate = null;
		this.southGate = null;
		this.westGate = null;
		this.objects = [];
	}

	setNortGate(northGate) {
		if (this.northGate != null) {
			this.northGate.removeAllListeners();
			this.objects.delete(this.northGate);
		}
		this.northGate = northGate;
		if (this.northGate == null)
			return;
		this.objects.push(this.northGate);
		this.northGate.removeAllListeners();
		this.northGate.onCollisionEnter = () => stageLoader.loadStage(stageLoader.x, stageLoader.y - 1, "south");
	}

	setEastGate(eastGate) {
		if (this.eastGate != null) {
			this.eastGate.removeAllListeners();
			this.objects.delete(this.eastGate);
		}
		this.eastGate = eastGate;
		if (this.eastGate == null)
			return;
		this.objects.push(this.eastGate);
		this.eastGate.removeAllListeners();
		this.eastGate.onCollisionEnter = () =>
			stageLoader.loadStage(stageLoader.x + 1, stageLoader.y, "west");
	}

	setSouthGate(southGate) {
		if (this.southGate != null) {
			this.southGate.removeAllListeners();
			this.objects.delete(this.southGate);
		}
		this.southGate = southGate;
		if (this.southGate == null)
			return;
		this.objects.push(this.southGate);
		this.southGate.removeAllListeners();
		this.southGate.onCollisionEnter = () => stageLoader.loadStage(stageLoader.x, stageLoader.y + 1, "north");
	}

	setWestGate(westGate) {
		if (this.westGate != null) {
			this.westGate.removeAllListeners();
			this.objects.delete(this.westGate);
		}
		this.westGate = westGate;
		if (this.westGate == null)
			return;
		this.objects.push(this.westGate);
		this.westGate.removeAllListeners();
		this.westGate.onCollisionEnter = () => stageLoader.loadStage(stageLoader.x - 1, stageLoader.y, "east");
	}

	draw() {
		if (this.northGate != null)
			this.northGate.draw();
		if (this.eastGate != null)
			this.eastGate.draw();
		if (this.southGate != null)
			this.southGate.draw();
		if (this.westGate != null)
			this.westGate.draw();
		for (let stageObject of this.objects)
			stageObject.draw();
	}

}