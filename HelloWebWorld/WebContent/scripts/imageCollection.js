class ImageCollection {
	constructor(list, callback) {
		var total = 0;
		var images = {};
		for (var i = 0; i < list.length; i++) {
			var img = new Image();
			images[list[i].name] = img;
			img.onload = function() {
				total++;
				if (total == list.length) 
					callback && callback();
			};
			img.src = list[i].url;
		}
		this.get = function(name) {
			return images[name] || (function() { throw "Not exist"; })();
		};
	}
}
