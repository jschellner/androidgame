const pressedKeys = new Set();

function onKeyPressed(e) {
	pressedKeys.add(e.key);
}

function onKeyReleased(e) {
	pressedKeys.delete(e.key);
}

function onMousePressed(e) {
	for (let gameObject of gameObjects)
		if (gameObject.onClick != null && rectContainsPoint(gameObject, e.x, e.y))
			gameObject.onClick();
	player.inventory.onMousePressed(e);
}