class Item {
	constructor(name, amount, maxStackSize, draw, onUse, consumable) {
		this.name = name;
		this.amount = amount;
		this.maxStackSize = maxStackSize;
		this.draw = draw;
		this.onUse = onUse;
		this.consumable = consumable;
	}

	activate() {
		if (this.onUse != null)
			this.onUse();
		if (!this.consumable)
			return;
		this.reduceAmount(1);
	}
	
	reduceAmount(value){
		this.amount -= value;
		if (this.amount <= 0)
			player.inventory.items.splice(player.inventory.items.indexOf(this), 1);
	}
}

function createItem(name, amount) {
	if (amount < 1)
		return;
	switch (name) {
		case "apple":
			return new Item("Apple", amount, 10,
				(x, y) => canvas.canvasContext.drawImage(images.get("apple"), x, y, itemSize, itemSize),
				() => player.heal(3), true);

		case "iron_ore":
			return new Item("Iron Ore", amount, 10,
				(x, y) => canvas.canvasContext.drawImage(images.get("iron_ore"), x, y, itemSize, itemSize),
				null, false);
				
				case "iron_sword":
			return new Item("Iron Sword", amount, 1,
				(x, y) => canvas.canvasContext.drawImage(images.get("iron_sword"), x, y, itemSize, itemSize),
				() => player.equippedWeapon = this, false);
	}
}