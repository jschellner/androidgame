class StageObject {
	constructor(name, x, y, width, height, blocksMovement, draw, isStatic = true) {
		this.name = name;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.blocksMovement = blocksMovement;
		this.onCollision = null;
		this.onClick = null;
		this.draw = draw;
		this.playerContained = false;
		this.onCollisionEnter = null;
		this.onCollisionExit = null;
		this.isStatic = isStatic;
	}

	collides(x, y, width, height, fireEvents) {
		let result = x < this.x + this.width &&
			x + width > this.x &&
			y < this.y + this.height &&
			y + height > this.y;
		if (!result) {
			if (fireEvents && this.playerContained && this.onCollisionExit != null)
				this.onCollisionExit();
			this.playerContained = false;
			return false;
		}
		if (fireEvents && this.onCollision != null)
			this.onCollision();
		if (!this.playerContained) {
			if (fireEvents && this.onCollisionEnter != null)
				this.onCollisionEnter();
			this.playerContained = true;
		}

		return true;
	}

	removeAllListeners() {
		this.onCollision = null;
		this.onClick = null;
		this.onCollisionEnter = null;
		this.onCollisionExit = null;
	}
}