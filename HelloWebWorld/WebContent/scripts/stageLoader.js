class StageLoader {
	constructor() {
		this.x = 0;
		this.y = 0;
		this.stages = new Map();
		this.currentStage = null;
	}

	init() {
		let stage = new Stage();
		stage.setWestGate(new StageObject("sceneChangerLeft", 0, 100, 50, 50, false, drawRect));
		stage.setEastGate(new StageObject("sceneChangerRight", 800, 100, 50, 50, false, drawRect));
		stage.objects.push(new StageObject("stone", 200, 200, 100, 100, true, function() {
			canvas.canvasContext.drawImage(images.get("stone"), this.x, this.y, this.width, this.height);
		}));

		let temp = new StageObject("anvil", 400, 100, 100, 100, false, function() {
			canvas.canvasContext.drawImage(images.get("anvil"), this.x, this.y, this.width, this.height);
		});
		temp.onClick = () => {
			for (let item of player.inventory.items)
				if (item.name == "Iron Ore") {
					item.reduceAmount(1);
					player.inventory.items.push(createItem("iron_sword", 1));
					break;
				}
		};
		stage.objects.push(temp);
		this.stages.set("0;0", stage);
		this.currentStage = stage;

		stage = new Stage();
		stage.setWestGate(new StageObject("clickableNonBlocker", 0, 100, 50, 100, false, drawRect));
		stage.setEastGate(new StageObject("clickableNonBlocker", 600, 100, 50, 100, false, drawRect));
		stage.objects.push(new StageObject("blocker", 400, 200, 100, 100, true, drawRect));
		this.stages.set("-1;0", stage);
	}

	loadStage(x, y, newStagePlayerPosition) {
		let temp = this.stages.get(x + ";" + y);
		if (temp == undefined)
			return;
		this.currentStage = temp
		this.x = x;
		this.y = y;
		let gate = this.currentStage.northGate;
		switch (newStagePlayerPosition) {
			case "east":
				gate = this.currentStage.eastGate;
				break;
			case "south":
				gate = this.currentStage.southGate;
				break;
			case "west":
				gate = this.currentStage.westGate;
				break;
		}
		player.x = gate.x;
		player.y = gate.y;
		gate.playerContained = true;

		gameObjects = [];
		gameObjects.push(player);
		for (let temp of this.currentStage.objects)
			gameObjects.push(temp);
	}

}

function drawRect() {
	canvas.canvasContext.fillRect(this.x, this.y, this.width, this.height);
}