package de.schellner.websocket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.ServerHandshake;
import org.java_websocket.server.WebSocketServer;

public class Main {

	public static void main(String[] args) throws InterruptedException, IOException, URISyntaxException {
//		WebSocketServer server = new WebSocketServer(new InetSocketAddress("localhost", 3000)) {
//
//			@Override
//			public void onStart() {
//				System.out.println("server.onStarted");
//			}
//
//			@Override
//			public void onOpen(WebSocket conn, ClientHandshake handshake) {
//				System.out.println("server.onOpen: " + conn);
//			}
//
//			@Override
//			public void onMessage(WebSocket conn, String message) {
//				System.out.println("server.onMesage: " + message);
//			}
//
//			@Override
//			public void onError(WebSocket conn, Exception ex) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void onClose(WebSocket conn, int code, String reason, boolean remote) {
//				// TODO Auto-generated method stub
//
//			}
//		};
//		server.start();
//
//		new Thread(() -> {
//			BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
//			while (true) {
//				String in;
//				try {
//					in = sysin.readLine();
//					server.broadcast(in);
//					if (in.equals("exit")) {
//						server.stop(1000);
//						break;
//					}
//				} catch (IOException | InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}).start();
//		WebSocketClient client = new WebSocketClient(new URI("ws://games.owbgames.de:3000")) {
		WebSocketClient client = new WebSocketClient(new URI("ws://localhost:3000")) {

			@Override
			public void onOpen(ServerHandshake handshakedata) {
				System.out.println("client.onOpen: " + handshakedata);
				send("ballo");
			}

			@Override
			public void onMessage(String message) {
				System.out.println("client.onMessage: " + message);
			}

			@Override
			public void onError(Exception ex) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
				// TODO Auto-generated method stub

			}
		};
		System.out.println(client.connectBlocking());
		
		new Thread(() -> {
			BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String in;
				try {
					in = sysin.readLine();
					client.send(in);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}

}
